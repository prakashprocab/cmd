<?php

/**
 * Define the cmd text domain.
 */
! defined('CMD_TEXT_DOMAIN')? define('CMD_TEXT_DOMAIN','cmdbalexert'): CMD_TEXT_DOMAIN;

/**
 * Initialize the acf customize function class
 */
use \Admin\AcfCustomize\CmdAdmin as acfCustomizeInit;
new acfCustomizeInit();

/*
 *
 * Set the content width based on the theme's design and stylesheet.
 *
 * @since codalis 1.0
 */

if (! function_exists('procab_setup')) :
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     *
     * @sinceprocab 1.0
     */

    function procab_setup(){
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based onprocab, use a find and replace
        * to change 'procab' to the name of your theme in all the template files
        */

        load_theme_textdomain('app');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
         */

        add_theme_support('post-thumbnails');
        set_post_thumbnail_size(158, 240, true);


        // Registers a single custom Navigation Menu in the custom menu editor
        //  register_nav_menu('primary', __('Primary Menu', 'procab'));

        register_nav_menus( array(
            'primary_menu' => __( 'Main Menu', CMD_TEXT_DOMAIN ),
            'footer_menu' => __( 'Footer Menu', CMD_TEXT_DOMAIN )

        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */

        add_theme_support('html5', array(
            'search-form',
            'gallery',
            'caption'
        ));

        /*
   * Enable support for Post Formats.
   *
   * See: https://codex.wordpress.org/Post_Formats
   */
        add_theme_support( 'post-formats', array(
            'aside',
            'image',
            'video',
            'quote',
            'link',
            'gallery',
            'audio',
        ) );

    }

endif; //procab_setup
add_action('after_setup_theme', 'procab_setup');

/**
 * Register widget area.
 *
 * @since procab 1.0
 *
 * @link https://codex.wordpress.org/Function_Reference/register_sidebar

 */

function procab_widgets_init(){

    register_sidebar(array(
        'name' => __('Widget Area', 'app'),
        'id' => 'sidebar-1',
        'description' => __('Add widgets here to appear in your sidebar.', 'app'),
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>'

    ));

}

add_action('widgets_init', 'procab_widgets_init');
/**
 * Enqueue scripts and styles.
 *
 * @since procab 1.0
 */

function procab_scripts() {
    wp_enqueue_style('font_stylesheet', 'https://fonts.googleapis.com/css?family=Lato:300,400,400i,700|Playfair+Display:400,700|Raleway:300,400,400i,500,700', array(), '1.0');
    wp_enqueue_style('stylesheet', get_template_directory_uri() . '/assets/css/custom/style.min.css', array(), '1.0');

    wp_register_script('cmdMainScripts',get_template_directory_uri().'/assets/js/custom/cmd-main.min.js', array(), '1.0', true);

    wp_enqueue_script(array(
        'cmdMainScripts',
    ));
}

add_action('wp_enqueue_scripts', 'procab_scripts');


/*
** Add custom classes on body
*/
/*
function app_body_class($classes) {
    $classes[] = '';
    if(is_home() || is_front_page()) {
        $classes[] ='home page';
    }else{
        $classes[] = "inner-page is-request-quote-shown";
    }

    return $classes;
}
add_filter('body_class', 'app_body_class');*/