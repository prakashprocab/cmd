<?php
use Admin\RegisterPostTypes\Procab_Add_Post_Type as add_post_types;
use Admin\RegisterPostTypes\Procab_Add_Taxonomy as add_taxonomy;

if ( ! defined( 'ABSPATH' ) )
	exit;

add_action( 'init', function() {

	/**
	 * Register soins custom post type
	 */
	new add_post_types(
		'soins',
		[
			'has_archive' => false,
			'public' => true,
			'show_ui' => true,
			'menu_icon' => 'dashicons-heart',
			'supports' => [
				'title',
			],
			'rewrite' => [ 'slug'=>'soins' ],
			'menu_position' => 24,
            'publicly_queryable'  => true
		],
		esc_html__( 'Soins', CMD_TEXT_DOMAIN ),
		esc_html__( 'Soins', CMD_TEXT_DOMAIN )
	);
	//$name, $post_type, $args, $label, $singular_label = ''

	new add_taxonomy(
		'soins-categories',
		'soins',
		[
			'hierarchical'      => true,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'			=> ['slug' => 'categories'],
		],
		esc_html__( 'Categories', CMD_TEXT_DOMAIN ),
		esc_html__( 'Categories', CMD_TEXT_DOMAIN )
	);
	
	/**
	 * Register praticiens custom post type
	 */
	new add_post_types(
		'praticiens',
		[
			'has_archive' => false,
			'public' => true,
			'show_ui' => true,
			/*'capabilities' => array(
			    'create_posts' => false,//'do_not_allow', // false < WP 4.5, credit @Ewout
			),			
			'map_meta_cap' => true,*/
			'menu_icon' => 'dashicons-groups',
			'supports' => [
				'title',
			],
			'rewrite' => ['slug'=>'praticiens'],
			'menu_position' => 25,
            'publicly_queryable'  => false
		],
		esc_html__( 'Praticiens', CMD_TEXT_DOMAIN ),
		esc_html__( 'Praticiens', CMD_TEXT_DOMAIN )
    );

    /**
     * Register praticiens custom post type
     */
    new add_post_types(
        'praticiens',
        [
            'has_archive' => false,
            'public' => true,
            'show_ui' => true,
            'menu_icon' => 'dashicons-groups',
            'supports' => [
                'title',
            ],
            'rewrite' => ['slug'=>'praticiens'],
            'menu_position' => 25,
            'publicly_queryable'  => false
        ],
        esc_html__( 'Praticiens', CMD_TEXT_DOMAIN ),
        esc_html__( 'Praticiens', CMD_TEXT_DOMAIN )
    );
	


});
