<?php
/**
 * Created by PhpStorm.
 * User: Prakash Sunuwar
 * Date: 1/28/2019
 * Time: 5:02 PM
 * This class for customize the acf
 */
namespace Admin\AcfCustomize;

if ( ! defined( 'ABSPATH' ) )
    exit;

class CmdAdmin {

    public function __construct()
    {
        $this->_initializeAllHooks();
    }

    public function _initializeAllHooks()
    {
        /**
         * hide ACF dashboard on admin
         */
        #require WPMU_PLUGIN_DIR.'/advanced-custom-fields-pro/cpt.php';

        /**
         * https://www.advancedcustomfields.com/blog/acf-pro-5-5-13-update/
         * Faster load times!
         * disabe loading of scf custom tags
         */
        add_filter('acf/settings/remove_wp_meta_box', '__return_true');


        /**
         * add options page
         */
        if (function_exists('acf_add_options_page')) {
            acf_add_options_page(array(
                'page_title' => 'Header',
                'menu_title' => 'Header',
                'redirect' => false
            ));
            acf_add_options_page(array(
                'page_title' => 'Footer',
                'menu_title' => 'Footer',
                'redirect' => false
            ));
        }
    }
}// End of class

