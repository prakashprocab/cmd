<div class="sticky-messenger-holder">
    <a href="" class="shape-circle sticky-messenger-btn"><i class="icon icon-messenger icon-lg mr-0"><span class="path1"></span><span class="path2"></span></i></a>
</div>
<div class="scroll-top">
    <button class="scroll-top__node js-scroll-top-trigger shape-circle" role="button" type="button"><i class="icon icon-up-chev fw-b m-0"></i></button>
</div>