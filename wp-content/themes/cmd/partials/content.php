<?php
/*
if(!\App\is_post_content_empty($post)):

  ?>
   <section id="section-target-2" class="block block-cms block-simple-text theme-bkg-light box-stretch box-gap-eq ">
       <div class="block__body wow slideInUp" data-wow-duration="1s" data-wow-delay="0.15s" data-wow-offset="20" style="visibility: visible; animation-duration: 1s; animation-delay: 0.15s; animation-name: slideInUp;">
           <div class="text-container text-container--sm">
               <?php
               the_title('<h1>','</h1>');
               if(has_post_thumbnail()){
                   the_post_thumbnail('large', ['class'=>'img-responsive']);
               }
               the_content();

               ?>
           </div>
       </div>
   </section>
<?php  endif;*/ ?>

<?php
///*

/**
 * close section </div when there is atleast one section
 */
/*
function closeAccordionSection(){
    global $accordionHeadCount;
    if($accordionHeadCount > 0) echo '</div>';
    include __DIR__.'/flexible-contents/utility/section-js.php';
}
*/

//$flexCon = get_field('flexible_contents',$post->ID);
//var_dump($flexCon);
if( have_rows('flexible_contents') ):
    // loop through the rows of data
    global $accordionHeadCount;
    $accordionHeadCount = 0;
    $sections = [];

    while ( have_rows('flexible_contents') ) : the_row();
        $layout = get_row_layout();
        //$tpl = __DIR__.'/flexible-content/cms/'.$layout;
        $tpl = __DIR__.'/flexible-contents/'.$layout.'.php';

        if($layout == 'section'){
            $accordionHeadCount++;
            //include $tpl;
        }else{
            //echo '==DATA==';
        }
        ///*
        if(file_exists($tpl)) {
            include $tpl;
        }
        endwhile;
    //closeAccordionSection();
    if($accordionHeadCount > 0) echo '</div>';
    include __DIR__.'/flexible-contents/utility/section-js.php';
endif;


