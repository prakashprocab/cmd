<?php
/**
 * Created by PhpStorm.
 * User: laxman
 * Date: 7/8/2017
 * Time: 9:47 PM
 */
/**
 * add wrapper - template inheritance in wp
 */
add_filter('template_include', ['Procab\\Wp\\Wrapper', 'wrap'], 200);

function procab_wp_title( $title, $sep ) {
    global $paged, $page;
    if ( is_feed() ) {
        return $title;
    }
    // Add the site name.
    $title .= get_bloginfo( 'name', 'display' );
    // Add the site description for the home/front page.
    $site_description = get_bloginfo( 'description', 'display' );
    if ( $site_description && ( is_home() || is_front_page() ) ) {
        $title = "$title $sep $site_description";
    }
    // Add a page number if necessary.
    if ( ( $paged >= 2 || $page >= 2 ) && ! is_404() ) {
        $title = "$title $sep " . sprintf( __( 'Page %s', 'RJ' ), max( $paged, $page ) );
    }
    return $title;
}
add_filter( 'wp_title', 'procab_wp_title', 10, 2 );

//remove stress characters, stress characters
add_filter('wp_handle_upload_prefilter', 'procab_upload_filter',1,1 );
function procab_upload_filter( $file ){
    $file['name'] = preg_replace('/[^a-zA-Z0-9-_\.]/','-', $file['name']);
    return $file;

}
/*
 * Redirect to home page if author is 1
 */
add_action('template_redirect', 'procab_template_redirect');
function procab_template_redirect(){
    if (is_author()){
        wp_redirect( home_url() ); exit;
    }
}
//remove_filter( 'the_content', 'wpautop' );
//add_filter( 'the_content', 'wpautop' , 12);
/*
 * Modify Login
 */

function procab_login_logo() {
    echo '<style type="text/css">
        @import('.get_bloginfo( 'template_directory' ) .'/assets/fonts/admin-login-css/style.css);
        
       /* body{
            background: #e02146;
        }*/
        .login h1 a { background-image:url('. get_bloginfo( 'template_directory' ) .'/assets/images/logo.png) !important;background-size: 82% !important; margin-bottom: 20px; height: 55px; width: 162px; }
        .login h1 a:hover,
        .login h1 a:focus {
            box-shadow: none;
            outline: 0;
        }
        #loginform {
            background: #0dace2;            
        }

        #loginform .button-primary {
            background-color: #000;
            box-shadow: none;
            transform: none;
            text-shadow: none;
        }
        #loginform .button-primary:focus,
        #loginform .button-primary:active, {
        #loginform .button-primary:active:focus
            box-shadow: none;
            transform: none;
        }
        .login form .input, .login input[type=text], input[type=password] {
            display: block;
            margin-top: 10px;
            padding: 10px 20px;
            background: #0dace2;
            border: 1px solid #ffffff;
            color: #ffffff;
            font-size: 16px;            
        }
        .login form .input:focus, .login input[type=text]:focus, input[type=password]:focus {
            outline: 0;
            box-shadow: none;
        }
        .login label {        
            color: #ffffff;
            font-size: 16px;
        }
        input[type=text]:focus, input[type=password]:focus {
            outline: 0;
        }
        input:-webkit-autofill {
            -webkit-text-fill-color: #ffffff !important;
            -webkit-box-shadow: 0 0 0 1000px #0dace2 inset !important;
        }
        input::-webkit-input-placeholder { / Chrome/Opera/Safari /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input::-moz-placeholder { / Firefox 19+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-ms-input-placeholder { / IE 10+ /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }
        input:-moz-placeholder { / Firefox 18- /
            color: #333333;
            font: 15px / 1.2 "Aaux", "Arial", "Helvetica", sans-serif;
        }

        .login a {
            color: #393939;
            transition: all linear 0.35s;
        }

        .login #nav a ,
        .login #backtoblog a {
            color: #393939;
            font-size: 16px;
            font-weight: 600;
        }

        .login #backtoblog a {
            color: #0dace2;
        }

        .login #nav a:hover,
        .login #nav a:focus,
        .login #backtoblog a:hover,
        .login #backtoblog a:focus {
            color: #343a40;
            box-shadow: none;
            outline: 0;
        }

        .login form .forgetmenot label {
            font-size: 14px;
        }

    </style>';
}
add_action('login_head', 'procab_login_logo');

/*
* OPen external links in a new tab
*/
add_action( 'wp_head', 'procab_openlinks_innnewtab' );
function procab_openlinks_innnewtab() { ?>
    <script type="text/javascript">// <![CDATA[ javascript:void(0);
        jQuery(document).ready(function($){
            $('a[href]:not([href^="<?php echo site_url(); ?>"]):not([href^="#"]):not([href^="/"]):not([href*="javascript:void(0)"]):not([href^="tel"]):not([href^="mailto"]):not([href=""])').attr( 'target', '_blank' );
        });
        // ]]></script>
    <?php
}

/*
 *
 */
add_action( 'wp_ajax_display_contents', 'display_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_contents', 'display_contents_callbck' );
function display_contents_callbck() {
    extract($_GET);
    include __DIR__ . '/../partials/flexible-contents/filter-by-news/fliter-content.php';
    die();
}
/*
 *
 */

/*
 *
 */
add_action( 'wp_ajax_display_technology_contents', 'display_technology_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_technology_contents', 'display_technology_contents_callbck' );
function display_technology_contents_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/flexible-contents/filter-by-technology/fliter-content.php';
    die();
}

/*
 *
 */

add_action( 'wp_ajax_display_member_contents', 'display_member_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_member_contents', 'display_member_contents_callbck' );
function display_member_contents_callbck() {
    extract($_GET);
    include __DIR__ . '/../partials/flexible-contents/filter-by-member/fliter-content.php';
    die();
}
/*
 * display_casestudy_contents
 */
add_action( 'wp_ajax_display_casestudy_contents', 'display_casestudy_contents_callbck' );
add_action( 'wp_ajax_nopriv_display_casestudy_contents', 'display_casestudy_contents_callbck' );
function display_casestudy_contents_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/flexible-contents/filter-by-case-study/fliter-content.php';
   // var_dump($_GET);
    die();
}

/*
 *
 */
add_action( 'wp_ajax_display_contents_by_tag', 'display_contents_by_tag_callbck' );
add_action( 'wp_ajax_nopriv_display_contents_by_tag', 'display_contents_by_tag_callbck' );
function display_contents_by_tag_callbck() {
    extract($_GET);
    include __DIR__.'/../partials/flexible-contents/filter-by-tags/fliter-content-by-tag.php';
    die();
}


function app_mce_buttons_2($buttons) {
    array_unshift($buttons, 'styleselect');
    return $buttons;
}
add_filter('mce_buttons_2', 'app_mce_buttons_2');
/*
* Callback function to filter the MCE settings
*/

function app_mce_before_init_insert_formats( $init_array ) {
    $style_formats = [
        [
            'title' => 'Button Box',
            'selector' => 'a',
            'classes' => 'btn btn-gradient-primary'
        ],
        [
            'title' => 'Underline Button',
            'selector' => 'a',
            'classes' => 'btn-link btn-link-primary'
        ],


    ];
    $init_array['style_formats'] = json_encode( $style_formats );
    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'app_mce_before_init_insert_formats' );

/*
 * Audio
 */
/*
 * Modifying the exisitng Media Player
 */

add_filter( 'wp_audio_shortcode_override', 'kd_audio_shortcode', 10, 4 );
function kd_audio_shortcode( $return = '', $attr, $content, $instances )
{
    # If $return is not an empty array, the shorcode function will stop here printing OUR HTML
    // $return = '<html code here>';
    if(!empty($attr['mp3'])) :

        $audioContents = ' <div class="ui-media">
                            <audio controls="controls" preload="auto" class="ui-media__node js-audio-node">
                                <!-- <source src="audio/minions-clip.ogg" type="audio/ogg" /> -->
                                <source src="'.$attr['mp3'].'" type="audio/mpeg" /></audio></div>';
    endif;

    return $audioContents;
};
/*
 *
 */

// ALL POST TYPES: posts AND custom post types
add_filter('manage_posts_columns', 'kd_columns_head');
//add_action('manage_posts_custom_column', 'ST4_columns_content', 10, 2);
// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function kd_columns_head($columns) {
    $columns['image_type'] = 'Image Type';
    return $columns;
}

add_action('manage_posts_custom_column', 'kd_image_type_custom_columns', 10, 2);
function kd_image_type_custom_columns($column, $post_id){
    global $post;
    switch ($column) {
        case 'image_type' :
            the_post_thumbnail('thumbnail');
            break;
    }
}