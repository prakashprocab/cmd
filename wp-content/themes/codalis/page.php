<?php
		while ( have_posts() ) : the_post();
            the_content();
            $blocks = get_field('flexible_contents');
          //  var_dump($blocks);
            if($blocks):
                foreach ($blocks as $block):
                    $tpl = __DIR__.'/partials/flexible-contents/'.strtolower($block['acf_fc_layout']).'.php';
                  if (file_exists($tpl)) {
                    include $tpl;
                  }
                endforeach;
            endif;
        endwhile;
