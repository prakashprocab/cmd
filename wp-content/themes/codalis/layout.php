<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
<?php get_template_part('partials/head'); ?>
 <!-- Add has-top-gap class in inner pages -->
<body <?php body_class(); ?>>
<!--[if ltE IE 9]>
<div class="alert alert-danger ie-older-browser-message">
    <div class="ie-message-text">
        <p>You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your
            browser</a> to improve your experience.</p>
    </div>
</div>
<![endif]-->
<!-- Site outer -->
<div class="site-outer">
    <!-- Site header -->
    <?php
        do_action('get_header');
        get_template_part('partials/header');
    ?>
    <!-- Site main content wrapper -->

    <main id="main" class="main" role="main">
        <div class="content">
            <div class="container">

                <?php if ( ! is_home() && ! is_front_page() ) : ?>
                <div class="breadcrumb is-extended">
                    <div class="breadcrumb__body">
                        <ul class="breadcrumb-list" role="list">
                            <?php
                            if(function_exists('bcn_display')) {
                                bcn_display();
                            }
                            ?>

                            <!-- <li class="breadcrumb-list__item"><a href="" class="breadcrumb-list__link">Accueil</a></li>
                             <li class="breadcrumb-list__item"><a href="" class="breadcrumb-list__link">Lorem ipsum</a></li>
                             <li class="breadcrumb-list__item">Dolor sit amet it conecteur </li>-->
                        </ul>
                    </div>
                </div><!-- /.Breadcrumb navigation ends -->
                <?php endif; ?>

                <?php include \App\get_main_template(); ?>
            </div><!-- /.Site container ends -->
        </div><!-- /.Site content ends -->
    </main><!-- /.Site main content wrapper ends -->
    <?php get_template_part('partials/foot'); ?>
</div><!-- /.Site outer ends -->

<?php
    do_action('get_footer');
    get_template_part('partials/footer');
    wp_footer();
?>
</body>
</html>