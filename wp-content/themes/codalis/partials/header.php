<?php
/*
 * html head part
 */

/*use App\Menu\Primary;
use App\Menu\Secondary;*/
use Procab\Wp\Menu;
?>
<header id="mastHead" class="topnavbar topnavbar--is-sticky <?php echo (!is_home() && !is_front_page() ? 'is-stuck' : '') ?>" role="banner">
    <div class="topnavbar__c">
        <div class="container container--lg">
            <div class="d-flex align-items-center topnavbar-row">
                <div class="topnavbar-row__i topnavbar-row__i--l">
                    <figure class="brand-holder m-0">
                        <a href="<?php echo home_url('/'); ?>" class="brand">
                            <?php
                            $logo = get_field("logo","option");
                            $whiteLogo = !empty($logo['white_logo'])? $logo['white_logo']:get_template_directory_uri().'/assets/images/logo.svg';
                            $darkLogo = !empty($logo['dark_logo'])?  $logo['dark_logo'] : get_template_directory_uri().'/assets/images/logo-dark.svg';
                            $mobileLogo = !empty($logo['mobile_logo'])? $logo['mobile_logo']: get_template_directory_uri().'/assets/images/logo-mobile.svg';
                            ?>
                            <img alt="Codalis brand logo" class="brand__logo" src="<?=$whiteLogo?>" />
                            <img alt="Codalis brand logo" class="brand__logo brand__logo--secondary" src="<?=$darkLogo?>" />
                            <img alt="Codalis brand logo" class="brand__logo brand__logo--xs-screen d-block d-sm-none" src="<?=$mobileLogo?>" />
                        </a>
                    </figure>
                </div>

                <div class="topnavbar-row__i topnavbar-row__i--r">
                    <nav class="navbar d-flex align-items-center justify-content-end" role="navigation">
                        <div class="navbar__header">
                            <button class="navbar__toggle line-bars js-navbar-toggle" data-target="js-flyout-menu">
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                                <span class="line-bar"></span>
                            </button>
                        </div>
                        <div class="menu-container d-flex align-items-center justify-content-start">
                            <div class="js-flyout-menu menu-holder flex-grow-1">
                                <button class="navbar__toggle line-bars js-navbar-toggle" data-target="js-flyout-menu">
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                    <span class="line-bar"></span>
                                </button>
                                <ul id="menu" class="menu d-flex align-items-center justify-content-end" role="menu">

                                    <?php
                                    if ( has_nav_menu( 'primary_menu' ) ) :
                                        wp_nav_menu( array(
                                            'theme_location' => 'primary_menu',
                                            'items_wrap' => '%3$s',
                                            'container' => '',
                                            'container_class' => '',
                                            'container_id' =>'',
                                            'walker'  => new Menu()
                                    ) );
                                    endif;
                                    ?>
                                    <script>
                                       // if(!document.querySelector("#menu .is-shown")){
                                       if(!document.querySelector("#menu .is-active .is-shown")){

                                           document.querySelector("#menu .js-submenu-content-container").classList.add('is-shown');
                                            document.querySelector("#menu .submenu-category__item").classList.add('is-active');
                                        }
                                    </script>
                                </ul>
                            </div>
                        </div>

                        <div class="dropdown lang-dropdown">
                            <button class="dropdown-toggle has-no-caret text-uppercase" type="button" id="langDropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo ICL_LANGUAGE_CODE; ?></button>
                            <ul class="dropdown-menu text-left text-md-center" aria-labelledby="langDropdownMenuButton">
                                <?php
                                //$languages = apply_filters( 'wpml_active_languages', NULL, 'orderby=id&order=desc' );
                               //   $languages = icl_get_languages('skip_missing=N&orderby=id&order=desc');
                                $languages =  apply_filters( 'wpml_active_languages', array(), array( 'skip_missing' => 0, 'orderby' => 'code' ) );
                                if ( !empty( $languages ) ) {
                                    foreach( $languages as $l ) {
                                        ?>
                                        <li class="dropdown-item"><a href="<?=$l['url']?>" class="dropdown-link d-block"><?=strtoupper($l['language_code'])?></a></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ul>
                        </div>
                        <?php
                        $stickyLink = get_field('sticky_link','options');
                        if(!empty($stickyLink)):
                            $target = !empty($stickyLink['target'])?'target="_blank"':'';
                            ?>
                            <div class="sticky-request-quote-holder"> <a href="<?=$stickyLink['url']?>" <?=$target?> class="btn btn-lg btn-danger sticky-request-quote-btn js-sticky-request-quote-btn w-100"><span class="d-none d-sm-block"><?=$stickyLink['title']?></span> <i class="icon icon-request-quote mr-0 d-block d-sm-none"></i></a></div>
                        <?php endif; ?>
                    </nav>
                </div>
            </div>
        </div><!-- /.Main container ends -->
    </div><!-- /.Site header content ends -->

	<!-- Include custom navbar script which includes javascript code related to sticky header, menu open/close, submenu, mobile menu, etc -->
	<script src="<?php echo get_template_directory_uri().'/scripts/custom/ps-custom-navbar.js'; ?>"></script>
</header><!-- /.Site header ends -->