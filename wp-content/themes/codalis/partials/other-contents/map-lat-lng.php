<?php
$map = get_field('google_map','options');
$googleMapLink =  !empty(get_field('google_map_link','options')) ? get_field('google_map_link','options') : '';
if(!empty($map)):
$mapLatLng = $map['lat'].','.$map['lng'];
?>
<div class="map-card">
    <div class="map-card__node-holder">
        <div class="map-card__node js-map-node" data-map-latlan="<?=$mapLatLng?>" data-link="<?=$googleMapLink?>" data-zoom="16" data-is-grayscale="true" data-map-markers="<?php echo get_template_directory_uri(); ?>/assets/images/map-pin.png, <?php echo get_template_directory_uri(); ?>/assets/images/map-pin-sm.png"></div>
    </div>
</div><!-- /.Map module ends -->
<?php endif;