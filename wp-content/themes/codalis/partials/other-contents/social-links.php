<?php
$linkedin = get_field('linkedin','options');
$twitter = get_field('twitter','options');
$instagram = get_field('instagram','options');
$email = get_field('email','options')
?>
<div class="social-media">
    <ul class="social-media__list social-media__list--inline justify-content-center justify-content-sm-start">
        <?php if(!empty($linkedin)): ?>
            <li class="social-media__item"><a href="<?=$linkedin?>" target="_blank" ><i class="icon icon-linkedin icon-md m-0"></i></a></li>
        <?php endif; ?>
        <?php if(!empty($twitter)): ?>
            <li class="social-media__item"><a href="<?=$twitter?>" target="_blank"><i class="icon icon-twitter icon-md m-0"></i></a></li>
        <?php endif; ?>
        <?php if(!empty($instagram)): ?>
            <li class="social-media__item"><a href="<?=$instagram?>" target="_blank" ><i class="icon icon-instagram icon-md m-0"></i></a></li>
        <?php endif; ?>
        <?php if(!empty($email)): ?>
            <li class="social-media__item"><a href="mailto:<?php $email?>" target="_blank"><i class="icon icon-envelope icon-md m-0"></i></a></li>
        <?php endif; ?>
    </ul>
</div>
