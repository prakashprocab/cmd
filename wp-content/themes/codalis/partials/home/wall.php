<section class="block spacing-py-eq bg-faded is-extended">
    <?php
     $wallTitle = !empty(get_field('wall_title'))?get_field('wall_title') :'';
     if(!empty($wallTitle)) :
    ?>
    <header class="block__header">
        <h2 class="block__title has-line has-line--lg right"><?=$wallTitle?></span></h2>
    </header>
    <?php endif; ?>
    <div class="block__body">
        <div class="square-card-container">
            <div class="row square-card-row">
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card twitter-card bg-gradient-primary">
                        <div class="square-card__body">
                            <div class="twitter-card-content">

                                <?php // fetchTweets(count="10") ;

                                ?>

                                <?php fetchTweets( array( 'count ' => 1 ) ); ?>


                                <div class="twitter-tag mb-2"># cloud</div>
                                <p>La majorité des données des firmes suisses seront dans un cloud public en 2020</p>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg">
                            <div class="social-media line-height-1">
                                <button class="social-media__link text-white"><i class="icon icon-twitter icon-lg mr-0"></i></button>
                            </div>
                        </footer>
                        <?php
                            $twitter = !empty(get_field('twitter_link'))?get_field('twitter_link'):'';
                            $target ="";
                             if(!empty($twitter['target'])):
                            $target='target="_blank"';
                            endif;

                            if(!empty($twitter)):
                        ?>
                        <a href="<?=$twitter['url']?>"  <?=$target?> class="link-stacked"><?=$twitter['title']?></a>
                        <?php endif; ?>
                    </article>
                </div>

                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card instagram-card">
                        <figure class="square-card__picture zoom-effect-holder mb-0">
                            <img alt="Instagram Image" class="square-card__img img img-full zoom-effect" src="<?php echo get_template_directory_uri(); ?>/contents/home/wall/square-img01_370x370.jpg" />
                        </figure>
                        <footer class="square-card__footer square-card__footer--lg">
                            <div class="social-media line-height-1">
                                <button class="social-media__link text-white"><i class="icon icon-instagram icon-lg mr-0"></i></button>
                            </div>
                        </footer>
                        <?php
                        $instagram = !empty(get_field('instagram_link'))?get_field('instagram_link'):'';
                        $target ="";
                        if(!empty($instagram['target'])):
                            $target='target="_blank"';
                        endif;

                        if(!empty($instagram)):
                        ?>
                        <a href="<?=$instagram['url']?>"  <?=$target?> class="link-stacked"><?=$instagram['title']?></a>
                        <?php endif; ?>
                    </article>
                </div>

                <?php
                 $event= !empty(get_field('event'))?get_field('event'):'';
                if(!empty($event)):
                    $date = $event['date'];
                    //  $end_date     = get_field('end_date');
                    $month  = date_i18n( 'M', strtotime( $date ) );
                   $day = date_i18n( 'd', strtotime( $date ) );
                   $hideDate = !empty($event["hide_date"])?$event["hide_date"]:'';
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-4">
                    <article class="square-card event-card has-hover-action">
                        <div class="square-card-inner hover-effect-bubba text-white">
                            <?php
                            $eventImage = $event['image']['url'];
                            $eventImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($eventImage), \App\IMAGE_SIZE_EVENT);
                            if(!empty($eventImage) || $eventImage=='null'): ?>
                            <figure class="square-card__picture zoom-effect-holder mb-0">
                                <img alt="Event Image" class="square-card__img img img-full zoom-effect" src="<?php echo $eventImage; ?>" />
                            </figure>
                            <?php endif;?>

                            <?php if(empty($hideDate)): ?>
                            <footer class="square-card__footer right text-right">
                                <div class="ui-picto ui-picto--lg ui-picto--primary">
                                    <div class="ui-picto__row">
                                        <div class="ui-picto__item">
                                            <i class="icon icon-calendar icon-lg mr-0"></i>
                                        </div>
                                        <div class="ui-picto__item">
                                            <div class="h3 mb-0 event-date"><strong><?php echo $day; ?></strong></div>
                                            <div class="event-month"><?php echo $month; ?></div>
                                        </div>
                                    </div>
                                </div>
                            </footer>
                            <?php endif; ?>
                            <?php if(!empty($event['description']) || !empty($event['title'])): ?>
                            <div class="square-card__hover-content">
                                <div class="square-card__hover-content-inner">
                                    <div class="mb-2"><?=$event['title']?></div>
                                    <?php echo $event['description']; ?>
                                    <a href=" <?php echo $event['link']; ?>" class="link-stacked d-none"></a>
                                </div>
                            </div>
                            <?php endif; ?>
                        </div>
                    </article>
                </div>
                <?php endif;
                $partner = !empty(get_field('partners'))?get_field('partners'):get_field('partners');
                $partnerLogo = !empty($partner['logo'])?$partner['logo']:'';

                if(!empty($partnerLogo)):
                        $countPartner = count($partner['logo']);
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col mb-sm-0 mb-4">
                    <article class="square-card logo-card bg-info text-white js-counter-slider-parent">
                        <div class="square-card__body">
                            <?php if(!empty($partner['title'])): ?>
                                <h4 class="logo-card__title text-normal mb-4"><strong><?=$partner['title']?></strong></h4>
                            <?php endif; ?>
                            <div class="swiper-container counter-slider js-counter-slider">
                                <div class="swiper-wrapper">
                                    <?php foreach ($partnerLogo as $pLogo): ?>
                                    <div class="swiper-slide counter-slider__item">
                                        <figure class="counter-slider-picture text-center mb-0"><img alt="Partner1 Logo" class="img" src="<?php echo $pLogo['image']; ?>" /> </figure>
                                    </div>
                                    <?php endforeach;?>
                                </div>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg right text-right">
                            <div class="counter-controls">
                                <div class="swiper-button-prev js-counter-swiper-button-prev"><i class="icon icon-left-chev icon-white mr-0"></i></div>
                                <div class="swiper-button-next js-counter-swiper-button-next"><i class="icon icon-right-chev icon-white mr-0"></i></div>
                                <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?=$countPartner?></span></div>
                            </div>

                            <?php /*
 //Hidden for a moment
 if(!empty($partner)): */?><!--
                            <div class="ui-picto ui-picto--faded">
                                <i class="icon icon-plus text-info mr-0"></i>
                                <a href="<?/*=$partner['link']*/?>" class="link-stacked"></a>
                            </div>
                            --><?php /*endif;*/ ?>
                        </footer>
                    </article>
                </div>
                <?php endif;
                $client = get_field('clients');
                $clientLink = !empty($client["link"])?$client["link"] : "#";
                $clientLogo = !empty($client['logo'])?$client['logo']:'';
                if(!empty($clientLogo)):
                $countClient = count($client['logo']);
                ?>

                <div class="col-sm-4 col-xmd-6 squared-card-col mb-sm-0 mb-4">
                    <article class="square-card logo-card bg-gradient-primary text-white js-counter-slider-parent">
                        <div class="square-card__body">
                            <h4 class="logo-card__title text-normal mb-4"><strong>Nos clients</strong></h4>
                            <div class="swiper-container counter-slider js-counter-slider">
                                <div class="swiper-wrapper">
                            <?php foreach ($clientLogo as $cLogo): ?>
                                    <div class="swiper-slide counter-slider__item">
                                        <figure class="counter-slider-picture text-center mb-0"><img alt="Partner1 Logo" class="img" src="<?php echo $cLogo['image']; ?>" /> </figure>
                                    </div>
                            <?php endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <footer class="square-card__footer square-card__footer--lg right text-right">
                            <div class="counter-controls">
                                <div class="swiper-button-prev js-counter-swiper-button-prev"><i class="icon icon-left-chev icon-white mr-0"></i></div>
                                <div class="swiper-button-next js-counter-swiper-button-next"><i class="icon icon-right-chev icon-white mr-0"></i></div>
                                <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?php echo $countClient; ?></span></div>
                            </div>
                            <?php /*
     //Hidden for a moment
 if(!empty($clientLink)): */?><!--
                            <div class="ui-picto ui-picto--faded">
                                <i class="icon icon-plus text-success mr-0"></i>
                                <a href="<?/*=$clientLink*/?>" class="link-stacked"></a>
                            </div>
                            --><?php /*endif; */?>
                        </footer>
                    </article>
                </div>
                <?php endif;
                $blog = !empty(get_field('blog'))?get_field('blog'):'';
                if(!empty($blog)):
                ?>
                <div class="col-sm-4 col-xmd-6 squared-card-col">
                    <article class="square-card event-card has-hover-action">
                        <div class="square-card-inner hover-effect-bubba text-white">
                            <?php if(!empty($blog['image'])): ?>
                            <figure class="square-card__picture zoom-effect-holder mb-0">
                                <img alt="Event Image" class="square-card__img img img-full zoom-effect" src="<?php echo $blog['image']; ?>" />
                            </figure>
                            <?php endif; ?>
                            <div class="square-card__hover-content">
                                <div class="square-card__hover-content-inner">
                                    <?php if(!empty($blog['title'])): ?>
                                        <div class="mb-2"><?=$blog['title']?></div>
                                    <?php endif; ?>
                                    <?php if(!empty($blog['text'])):
                                            echo $blog['text'];
                                        endif;
                                    if(!empty($blog['link'])):
                                    ?>
                                        <a href="<?=$blog['link']?>" class="link-stacked d-none"></a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</section>