<section class="stacked-block stacked-block--promo spacing-py-eq pb-0">
    <div class="stacked-block__body">
        <?php $bannerImage = get_field('home_banner_image');
    //    $bannerImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($bannerImage), \App\IMAGE_SIZE_BANNER);
        if(!empty($bannerImage)):
        ?>
        <div class="stacked-promo is-extended-full">
            <figure class="stacked-promo__picture has-gradient-mask mb-0"><img alt="Image" class="stacked-promo__img img" src="<?php echo $bannerImage; ?>" /></figure>
        </div>
        <?php endif; ?>
        <div class="stacked-card-container">
            <?php
            $sliderImage = get_field('home_slider');
            $slider = [];
            if(!empty($sliderImage)):
                foreach ($sliderImage as $slide):
                    if(!empty($slide['image'])):
                      array_push($slider,$slide['image']);
                    endif;
                endforeach;
            endif;

            if(!empty($sliderImage[0]['image'])):
            ?>
            <div class="stacked-card-col stacked-card-col--picture mt-pushed-350">
                <div class="stacked-card is-shadowed">
                    <div class="stacked-card__body p-0">
                        <figure class="stacked-card__picture mb-0">
                            <img alt="Picture" class="stacked-card__img img" id="jsRandomizedImageNode" src="<?php echo $sliderImage[0]['image']; ?>" />
                        </figure>
                    </div>
                </div>
            </div>
            <?php endif; ?>

            <?php $quote = get_field('quote');
                if(!empty($quote)):
            ?>
            <div class="stacked-card-col stacked-card-col--text align-self-center">
                <div class="stacked-card stacked-card--sm is-xs-shadowed">
                    <div class="stacked-card__body">
                        <blockquote><p>« <?=$quote?> »</p></blockquote>
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </div>
    </div>

    <!-- Javascript code to randomize images on page load -->
    <script>
        (function() {
            var imagesArray = <?php echo JSON_encode($slider);?>;
            var usedImages = {};
            var usedImagesCount = 0;
            var jsRandomizedImageNode = document.getElementById('jsRandomizedImageNode');

            function ps_displayRandomImagesOnPageLoad () {

                var num = Math.floor(Math.random() * (imagesArray.length));
                if (!usedImages[num]){
                    jsRandomizedImageNode.src = imagesArray[num];
                    usedImages[num] = true;
                    usedImagesCount++;
                    if (usedImagesCount === imagesArray.length){
                        usedImagesCount = 0;
                        usedImages = {};
                    }
                } else {
                    ps_displayRandomImagesOnPageLoad() ;
                }
            }

            ps_displayRandomImagesOnPageLoad();
        })();
</script>

</section><!-- /.Home page services banner section  -->