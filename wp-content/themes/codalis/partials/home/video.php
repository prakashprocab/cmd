<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/10/2018
 * Time: 2:14 PM
 */
?>

<section class="hero hero--base has-video is-extended-full">
	<div class="hero__body">
		<!-- Hero video -->
		<div class="hero__video video-card-container overflow-hidden">
            <?php
            $videoBanner = get_field('video_banner');
            $videoBanner = \App\getImageManager()->resize(\App\getImageDirectoryPath($videoBanner), \App\IMAGE_SIZE_VIDEO_BANNER);
            if (!empty($videoBanner)):
                ?>
				<div class="video-card d-none d-sm-block">
					<div class="video-card__video-holder js-player-wrap" id="player-overlay-holder">
						<div class="video-card__overlay has-cover js-player-overlay" style="background-image:url(<?php echo $videoBanner; ?>);"></div>
						<div id="player"></div>
					</div>
				</div>
            <?php endif; ?>

            <?php
            $mobileVideoBanner = get_field('responsive_device_banner');
            if (!empty($mobileVideoBanner)):
                ?>
				<!-- Hero video fallback image in smaller screen -->
				<figure class="hero__pic d-block d-sm-none mb-0">
					<img alt="Codalis Video Banner" class="hero__img img img-full"
						 src="<?php echo $mobileVideoBanner; ?>"/>
				</figure><!-- /.Hero video fallback image ends -->
            <?php endif; ?>

            <?php
            $videoUrl = get_field('video_url');
            $youTubeVideoID = app\youtube_video_id($videoUrl);
            ?>
			<!-- Youtube video API library -->
			<script>
                // https://developers.google.com/youtube/iframe_api_reference
                // 2. This code loads the IFrame Player API code asynchronously.
                if (window.screen.availWidth > 767) {
                    var tag = document.createElement('script');
                    tag.src = "https://www.youtube.com/iframe_api";
                    var firstScriptTag = document.getElementsByTagName('script')[0];
                    firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
                }
                // 3. This function creates an <iframe> (and YouTube player)
                //    after the API code downloads.
                var player;
                var playerOverlay;

                // var videoClearInterval;

                function onYouTubeIframeAPIReady() {
                    playerOverlay = jQuery('.js-player-overlay');
                    player = new YT.Player('player', {
                        height: '100%',
                        width: '100%',
                        videoId: '<?=$youTubeVideoID?>', //'YT_VIDEO_ID',
                        playerVars: {
                            autoplay: 1,
                            modestbranding: 1,
                            rel: 0,
                            showinfo: 0,
                            controls: 0,
                            disablekb: 1,
                            enablejsapi: 0,
                            vq: 'hd1080',
                        },
                        events: {
                            'onReady': onPlayerReady,
                            'onStateChange': onPlayerStateChange
                        }
                    });
                }

                var nYoutubeInterval = -1;

                function getCurrentTime() {
                    player.getCurrentTime();
                }

                // 4.
                function onPlayerReady(event) {
                    event.target.playVideo();

                    nYoutubeInterval = setInterval(function () {

                        var time = player.getCurrentTime();
                        if (time == 0) {
                            player.mute();
                            player.playVideo();
                        }
                        if (time > 0.04) {
                            clearInterval(nYoutubeInterval);
                            playerOverlay.fadeOut();
                            //player.mute();
                        }

                    }, 50);
                }

                // 5. The API calls this function when the player's state changes.
                //    The function indicates that when playing a video (state=1),
                //    the player should play for six seconds and then stop.
                function onPlayerStateChange(event) {
                    if (event.data == 1) {
                        playerOverlay.fadeOut();
                        player.mute();
                    }

                    if (event.data == YT.PlayerState.ENDED) {
                        player.seekTo(0);
                    }
                }
			</script>
		</div>
        <?php

        $overlayContent = get_field("overlay_content");
        $display = $overlayContent['display'];
        $link = $overlayContent['link'];
        $title = $overlayContent['title'];
        $targetLink = !empty($link["target"])?'target="_blank"':'';
        if(!empty($display)):
            if(!empty($overlayContent)):
            ?>
            <div class="overlay-container">
                <div class="overlay-card hero__overlay-card right y-center bg-transparent-dark">
                    <div class="overlay-card__body">
                        <?php if(!empty($title)): ?>
                        <h3 class="overlay-card__title text-white"><?=$title?></h3>
                        <?php endif; ?>
                        <?php if(!empty($link['url'])): ?>
                             <a href="<?=$link['url']?>" class="btn btn-gradient-primary w-100" <?=$targetLink?>><?=$link['title']?></a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <?php endif;
        endif; ?>
	</div>
</section>