<?php
$serviceContents = get_field('services_contents');

//var_dump($serviceContents);
if(!empty($serviceContents)):
?>
<section class="stacked-block spacing-py-eq pb-0 is-extended">
<?php  $serviceTitle = get_field('service_title');
		if(!empty($serviceTitle)):
?>
		<header class="stacked-block__header">
			<h2 class="stacked-block__title has-line right"><?php echo $serviceTitle; ?></span></h2>
		</header>
<?php endif; ?>
		<div class="stacked-block__body">
			<?php
				$count=1;
				$figCaptionClass ='';
				$centerClass='';
				$order='';
				foreach($serviceContents as $serviceContent) :
					$imageCaption = $serviceContent['image_caption'];
					$button = $serviceContent['button'];
					if($count>1) {
						$centerClass = "align-items-center ";
					}
					if($count==2) {
						$figCaptionClass = "right ";
						$order = "order-sm-2 ";
					}else {
						$figCaptionClass = "";
						$order = "";
					}

					$imageLinks = $serviceContent['image_links'];
				//	var_dump($imageLinks);

					$figCaption = !empty($serviceContent['image_caption'])? $serviceContent['image_caption']:'';
			?>
			<div class="stacked-card-container stacked-card-container--child-pushed <?=$centerClass?>js-stacked-card-container">

				<div class="stacked-card-col stacked-card-col--picture <?=$order?>">
					<?php if(!empty($imageLinks[0]['image']['url'])): ?>
					<div class="stacked-card">
						<div class="stacked-card__body p-0">
							<figure class="stacked-card__picture has-cover mb-0 js-stacked-card-picture" style="background-image:url(<?php echo $imageLinks[0]['image']['url']; ?>);">
								<?php if(!empty($figCaption)): ?>
								<figcaption class="stacked-card__picture-caption overlay-card bottom <?=$figCaptionClass?> js-stacked-card-picture-caption text-faded"><?=$figCaption?></figcaption>
							<?php endif; ?>
							</figure>
						</div>
					</div>
				</div>
				<div class="stacked-card-col stacked-card-col--text mt-pushed-70">
					<div class="stacked-card is-shadowed bg-white">
						<div class="stacked-card__body">
							<?php if(!empty($serviceContent['title'])): ?>
							<h3 class="stacked-card__title"><?php echo $serviceContent['title'] ; ?></h3>
							<?php endif;
								if(!empty($imageLinks)):
							?>
							<ul class="lists lists--arrow lists--dark stacked-card__list js-synced-lists">
								<?php $c=1;
								foreach ($imageLinks as $imgLink):
									$active = ($c==1)?'is-active':'';
									 $imgUrl = $imgLink['image']['url'];
									 $imgUrl = \App\getImageManager()->resize( \App\getImageDirectoryPath($imgUrl), \App\IMAGE_SIZE_SERVICE);
									 $link = $imgLink['link'];
									$targetLink = !empty($link["target"])?'target="_blank"':'';
									if(!empty($link['url'])):
                                ?>
								<li class="lists__item <?=$active?>">
									<a href="<?=$link['url']?>" <?=$targetLink?> class="lists__link js-synced-lists-link" data-img-src="<?php echo $imgUrl; ?>"><?=$link['title']?></a>
								</li>
								<?php $c++;  endif;endforeach; ?>
							</ul>
							<?php endif; ?>
							<?php if(!empty($button)):
                               // var_dump($button['target']);
								$target = !empty($button["target"])?'target="_blank"':'';
								?>
							    <a href="<?=$button['url']?>" <?=$target?> class="btn btn-gradient-primary" role="button"><?=$button['title']?></a>
							<?php endif; ?>
						</div>
					</div>
				</div>
				<?php endif; ?>
			</div>
			<?php $count++; endforeach; ?>
		</div>
</section><!-- /.Home page services section ends -->
<?php endif;