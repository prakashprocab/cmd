<?php
$leftTitle = get_field('left_side_title');
$rightLink = get_field('right_side_link');
$stickyLink = get_field('sticky_link');
 if(!empty($leftTitle) || !empty($rightLink)) : ?>
    <section class="block block--intro mt-pushed-100 position-static is-extended">
        <div class="block__body">
            <div class="box-card-container box-card-container-pad-left bg-gradient-primary is-xs-extended js-request-quote-target">
                <div class="row no-gutters">
                    <?php if(!empty($leftTitle)) : ?>
                        <div class="col-sm-8 d-flex intro-col intro-col--lg">
                            <div class="box-card align-items-center">
                                <div class="box-card__body">
                                    <h2 class="mb-0"><?=$leftTitle?></h2>
                                </div>
                            </div>
                        </div>
                    <?php endif;

                    $target="";
                    if(!empty($rightLink)):
                        if(!empty($rightLink['target'])) :
                            $target='target="_blank"';
                        endif;
                        ?>
                        <div class="col-sm-4 d-flex intro-col intro-col--sm">
                            <div class="box-card box-card--md bg-danger has-hover-action align-items-center text-white text-center">
                                <div class="box-card__body">
                                    <a href="<?=$rightLink['url']?>" <?=$target?> class="link-stacked"></a>
                                    <h4 class="mb-0"><strong><?=$rightLink['title']?></strong></h4>
                                </div>
                            </div>
                            <?php
                        if(!empty($stickyLink)):
                            if(!empty($stickyLink['target'])) :
                                $target='target="_blank"';
                            endif;
                            ?>
                            <div class="sticky-request-quote-holder"><a href="<?=$stickyLink['url']?>" <?=$target?> class="btn btn-lg btn-danger sticky-request-quote-btn w-100"><?=$stickyLink['title']?></a></div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section><!-- /.Home page intro section ends -->
<?php endif;