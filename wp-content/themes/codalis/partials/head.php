<?php
/*
 * only meta head info
 */
?>
<head>
    <meta charset="<?php bloginfo( 'charset' );?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <!-- Place favicon.ico in the favicon directory -->
    <link rel="apple-touch-icon" href="icon.png">
    <?php  wp_head(); ?>
</head>


