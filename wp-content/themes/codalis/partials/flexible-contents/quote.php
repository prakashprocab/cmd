<?php

$bgColor = empty($block['quote_background_color'])?'bg-gradient-primary':$block['quote_background_color'];
$quote = $block['quote'];
if(!empty($quote)):
?>
<section class="block block--blockquote <?=$bgColor?> is-shadowed spacing-py-md is-extended">
    <div class="block__body">
        <div class="text-container blockquote-container">
            <aside class="blockquote-card text-center text-white">
                <blockquote class="blockquote-card__content h2 ff-lato"><p>“ <?=$quote?> ”</p></blockquote>
            </aside>
        </div>

    </div>
</section>
<?php endif;