<?php
$spaceType = empty($block['space_type'])?'spacing-py-md':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
?>
<div class="block block--filter <?=$spaceType?> <?=$bgColor?> is-extended">
    <div class="block__body">
        <nav class="filter-nav" role="navigation">
            <div class="filter-nav__label is-collapsed js-collapse-xs-toggle" data-panel-toggle="js-collapse-xs-panel"> <?php _e("Filtrer par","app"); ?></div>
            <form name="filter-form" class="form-filter" id="js-form-member">
                <ul class="filter-nav__list js-collapse-xs-panel" role="listbox">
                    <?php
                    $categories = get_categories( array(
                        'taxonomy' => 'member-category',
                        'orderby' => 'name',
                        'order'   => 'ASC'
                    ) );
                    foreach ($categories as $cat):
                        $termId = $cat->term_id;
                        $termName = $cat->name;
                        ?>
                        <li class="filter-nav__item">
                            <input class="d-none filter-field-input js-filter-checkbox" id="category<?=$termId?>" name="category[]" type="checkbox" value="<?=$termId?>">
                            <label class="btn filter-field-label" for="category<?=$termId?>"><?=$termName?></label>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </form>
        </nav>
        <!-- Script to add toggle feature in responsive mode -->
        <script>
            ;(function() {
                var collapseToggle = document.querySelector('.js-collapse-xs-toggle');
                var collapseTarget = '.' + collapseToggle.getAttribute('data-panel-toggle');

                if (collapseToggle) {
                    collapseToggle.addEventListener('click', function(e) {

                        if (collapseToggle.className.indexOf('is-collapsed') !== -1) {
                            collapseToggle.classList.remove('is-collapsed');
                            document.querySelector(collapseTarget).classList.add('is-shown');
                        } else {
                            collapseToggle.classList.add('is-collapsed');
                            document.querySelector(collapseTarget).classList.remove('is-shown');
                        }
                        e.preventDefault();
                    });
                }

                var checkboxes = document.querySelectorAll('.js-filter-checkbox');

                for (var i = 0, j = checkboxes.length; i < j; i++) {
                    checkboxes[i].addEventListener('change', function() {
                        var categories = [];
                        Array.prototype.forEach.call(checkboxes, function(elem) {

                            if ( elem.checked ) {
                                categories.push( elem.value );
                            }
                        });

                        // combine inclusive filters
                        var selectedCategories = categories.length ? categories.join(', ') : '';

                       // console.log(selectedCategories);
                        submitFormMember(selectedCategories);
                    });
                }

            })();
        </script>

    </div>
</div>



<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <?php if(!empty($title)): ?>
        <header class="block__header">
            <h2 class="stacked-block__title"><?=$title?></h2>
        </header>
    <?php endif; ?>
    <div class="block__body">
        <div id="loading-image" class="text-center">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/loader.gif" />
        </div>
        <div class="row js-row js-member-list">
        </div>

        <div class="col-12 not-found-member h3 text-danger text-center d-none" style="padding-bottom: 50px;"> <?php  _e("Content does not match the filter criteria","app"); ?></div>

</section>

    <script>

        function submitFormMember($catFilter) {
            var frmData = 'cat='+$catFilter +'&action=display_member_contents';
            var ajaxURL = "<?=admin_url('admin-ajax.php')?>";
            $.ajax({
                url: ajaxURL,
                data: frmData,
                beforeSend: function(){
                    // Show image container
                    $("#loading-image").show();
                },
                complete: function(){
                    $('#loading-image').hide();
                },
                success: function(response){
                    if (response.length > 0) {
                        $(".not-found-member").addClass("d-none");
                        $(".js-member-list").show();
                        $(".js-member-list").html(response);
                        // == Renit javascript codes related to these blocks
                        window.buildLightBoxVideo($('.js-lightbox')); // == Referenced from ps-script.js
                        window.getLastRowCols($('.js-member-list'), $('.js-last-col')); // == Referenced from ps-script.js
                    }else {
                      $(".js-member-list").hide();
                        $(".not-found-member").removeClass("d-none");
                    }
                }
            });
        }
        jQuery( document ).ready(function() {
            submitFormMember();
        });
    </script>
</section>
