<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/27/2018
 * Time: 5:16 PM
 */
$sliderImages = $block['images'];
$countImage = count($sliderImages);
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
if(!empty($sliderImages)):
?>
    <section class="block <?=$bgColor?> <?=$spaceType?>  is-extended">
        <div class="block__body">
            <div class="counter-carousel-parent js-counter-carousel-parent">
                <div class="swiper-container counter-carousel js-twohalf-counter-carousel" data-breakpoints-slides-number='{"desktop": 2, "mobile": 1}'>
                    <div class="swiper-wrapper">
                      <?php  foreach ($sliderImages as $image):
                            $img= $image['image'];
                             $imageLink = $image['image_link'];
                            $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($img), \App\IMAGE_SIZE_TWO_COLS);
                         if(!empty($img)): ?>
                            <div class="swiper-slide counter-slider__item">
                                <figure class="counter-slider-picture text-center mb-0">
                                    <?php if(!empty($imageLink)):?>
                                    <a href="<?=$imageLink?>"><img alt="Partner1 Logo" class="img" src="<?=$img?>" /></a>
                                 <?php else : ?>
                                        <img alt="Partner1 Logo" class="img" src="<?=$img?>" />
                                 <?php endif; ?>
                                </figure>
                            </div>
                        <?php endif;
                            endforeach;
                        ?>
                    </div>
                </div>
                <div class="counter-controls counter-controls--static mt-4 mt-sm-5 mx-auto">
                    <div class="swiper-button-prev js-counter-swiper-button-prev"><strong><i class="icon icon-left-chev icon-white mr-0"></i></strong></div>
                    <div class="swiper-button-next js-counter-swiper-button-next"><strong><i class="icon icon-right-chev icon-white mr-0"></i></strong></div>
                    <div class="counter-slider__count js-counter-slider-count"><span class="js-current-slide current-slide-node">1</span>&nbsp;&nbsp;/&nbsp;&nbsp;<span class="js-total-slide"><?=$countImage?></span></div>
                </div>
            </div>
        </div>
    </section>
    <?php
endif;