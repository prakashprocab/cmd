<section class="block block--map position-relative is-extended-full">
    <div class="block__body">
        <?php include  get_template_directory().'/partials/other-contents/map-lat-lng.php'; ?>
        <div class="overlay-container box-card-container text-sm-left">
            <div class="overlay-card y-center right w-100">
                <div class="col-sm-4 d-flex p-0 pl-sm-3 ml-auto">
                    <div class="box-card box-card--lg address-card bg-gradient-primary">
                        <div class="box-card__body">
                            <?php include  get_template_directory().'/partials/other-contents/address.php';
                            $class ='class="social-media__link"';
                            include  get_template_directory().'/partials/other-contents/social-links.php'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
