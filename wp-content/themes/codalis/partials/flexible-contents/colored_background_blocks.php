<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$contents = $block['contents'];
//echo 'Space type '.$spaceType .'<br/>'.$bgColor;
if(!empty($contents)):
?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <!-- Social media / Square card group -->
        <div class="square-card-container">
            <div class="row js-row">
                <?php
                foreach ($contents as $content) {
                    $type = $content['block_type'];
                     if($type=="blue") {
                         $bgColor = "bg-primary";
                     }else {
                         $bgColor = "bg-info";
                     }
                     $title = $content['title'];
                     $text = $content['text'];
                     $link = $content['link'];
                     ?>
                    <div class="col-sm-4 col-xmd-6 squared-card-col js-last-col">
                        <article class="square-card <?=$bgColor?>">
                            <div class="square-card__body center">
                                <?php if(!empty($title)): ?>
                                    <div class="mb-2"><?=$title?></div>
                                <?php endif; ?>
                                <?php if(!empty($text)):
                                    echo $text;
                                endif;
                                ?>
                            </div>
                            <footer class="square-card__footer right text-right">
                                <div class="ui-picto ui-picto--faded">
                                    <i class="icon icon-plus text-success mr-0"></i>
                                    <a href="<?=$link?>" class="link-stacked"></a>
                                </div>
                            </footer>
                        </article>
                    </div>
                <?php } ?>
            </div>
        </div><!-- /.Social media / Square card group ends -->
    </div>
</section>
<?php
endif;