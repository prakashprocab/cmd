<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/22/2018
 * Time: 4:03 PM
 */

include 'columns/col-function.php';

$type = $block['type'];
$text_1 = $block['text_1'];
$text_2 = $block['text_2'];
$text_3 = $block['text_3'];

if($type=="three-cols") {
    include 'columns/three-col.php';
}elseif($type=="one-third") {
    include 'columns/one-third.php';
}elseif ($type=='one-col') {
    include 'columns/one-col.php';
}elseif ($type=='two-third') {
    include 'columns/two-third.php';
}else {
    include 'columns/two-col.php';
}