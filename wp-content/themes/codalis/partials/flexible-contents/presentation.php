<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$presentationtText = $block['text'];
$keyNumbers = $block['key_numbers'];
if(!empty($presentationtText) || !empty($keyNumbers)):
?>
<!-- Presentation section :: CMS Module -->
<section class="block block--presentation <?=$spaceType?> <?=$bgColor?> is-extended">
    <div class="block__body">
        <div class="box-card-container bg-white is-shadowed text-lg fw-l">
            <div class="row no-gutters">
            <?php if(!empty($presentationtText)):?>
                <div class="col-sm-4 d-flex">
                    <div class="box-card box-card--md align-items-center">
                        <div class="box-card__body pr-sm-0">
                            <p><?=$presentationtText?> </p>
                        </div>
                    </div>
                </div>
        <?php endif; ?>
                <?php  $growClass = empty($presentationtText)?'flex-grow-1':''; ?>
                <div class="col-sm d-flex">
                    <div class="box-card box-card--md align-items-center text-center">
                        <div class="box-card__body">
                            <div class="row grid-v-border-separator">
                                <?php
                                $count=1;
                                $class='';
                                foreach ($keyNumbers as $num) :
                                    $class = ($count<3) ? 'mb-3 mb-sm-0' : '';
                                    ?>
                                    <div class="col-4 d-flex grid-col-styled">
                                        <div class="counter-card <?=$class?> <?=$growClass?>">
                                            <div class="counter-card__title h2 mb-0"><strong><?=$num['number']?></strong></div>
                                            <?php if(!empty($num['text'])) : ?> <p><?=$num['text']?></p> <?php endif; ?>
                                        </div>
                                    </div>
                                <?php $count++; endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.Presentation section ends -->
<?php endif;