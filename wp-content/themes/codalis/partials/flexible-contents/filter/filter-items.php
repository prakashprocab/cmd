<?php
$query = new WP_Query($args);
while($query->have_posts()): $query->the_post();
    $type = get_field('type');
    if($type=="video") :
        $class ="video-card";
    elseif (($type=='event')):
        $class ="event-card ";
    else :
        $class ="study-case-card ";
    endif;
    $hoverText = get_the_content();
    $title = get_the_title();
    $link = get_permalink();

    $image =  get_the_post_thumbnail_url( $post->ID, 'full' );
    $image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_THEMES_BLOCK);
    echo '<div class="col-sm-4 col-xmd-6 squared-card-col mb-4 js-last-col col-has-mb">
    <article class="square-card '.$class.' has-hover-action">
        <div class="square-card-inner hover-effect-bubba text-white">
            <figure class="square-card__picture zoom-effect-holder mb-0">
                <img alt="Event Image" class="square-card__img img img-full" src="'.$image.'">
                <a href="'.$link.'" class="link-stacked"></a>
            </figure>';
    $terms = wp_get_post_terms(get_the_ID(),'post_tag');
    if($type=="video") :
        $videoUrl = get_field('video_url');
        $youTubeVideoID = app\youtube_video_id($videoUrl);
        $caseStudyVideoLabel = get_field('case_study_video','options');
        ?>
        <footer class="square-card__footer right text-right">
            <div class="ui-picto ui-picto--lg ui-picto--info">
                <div class="ui-picto__row">
                    <div class="ui-picto__item">
                        <i class="icon icon-play-outline icon-lg mr-0"></i>
                    </div>
                    <?php if(!empty($caseStudyVideoLabel)): ?>
                        <div class="ui-picto__item">
                            <div class="h5 mb-0"><?=$caseStudyVideoLabel?></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </footer>
        <div class="square-card__hover-content">
            <div class="square-card__hover-content-inner">
                <?php if(!empty($title)): ?>
                    <div class="mb-2"><?=$title?></div>
                <?php endif; ?>
                <?php if(!empty($hoverText)) :
                    echo $hoverText;
                endif;
                ?>
                <div class="js-lightbox">
                    <a href="https://www.youtube.com/embed/<?=$youTubeVideoID?>" class="link-stacked js-lightbox-play"></a>
                </div>
            </div>
        </div>
    <?php elseif ($type=="event"): ?>
        <footer class="square-card__footer right text-right">
            <div class="ui-picto ui-picto--lg ui-picto--success">
                <div class="ui-picto__row">
                    <div class="ui-picto__item">
                        <i class="icon icon-calendar icon-lg mr-0"></i>
                    </div>
                    <div class="ui-picto__item">
                        <div class="h3 mb-0 event-date"><strong><?php echo get_the_date('d'); ?></strong></div>
                        <div class="event-month"><?php echo get_the_date('M'); ?></div>
                    </div>
                </div>
            </div>
        </footer>
        <div class="square-card__hover-content">
            <div class="square-card__hover-content-inner">
                <?php if(!empty($title)): ?>
                    <div class="mb-2"><?=$title?></div>
                <?php endif; ?>
                <?php if(!empty($hoverText)) :
                    echo $hoverText;
                endif;
                ?>
                <a href="<?=$link?>" class="link-stacked"></a>
            </div>
        </div>

    <?php elseif ($type=="study-case"):
        $caseStudyLabel = get_field('case_study_label','options');
        ?>
        <footer class="square-card__footer right text-right">
            <div class="ui-picto ui-picto--lg ui-picto--danger">
                <div class="ui-picto__row">
                    <div class="ui-picto__item">
                        <i class="icon icon-docs icon-lg mr-0"></i>
                    </div>
                    <?php if(!empty($caseStudyLabel)) : ?>
                        <div class="ui-picto__item">
                            <div class="h5 mb-0"><?=$caseStudyLabel?></div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </footer>
        <div class="square-card__hover-content">
            <div class="square-card__hover-content-inner">
                <?php if(!empty($title)): ?>
                    <div class="mb-2"><?=$title?></div>
                <?php endif; ?>
                <?php if(!empty($hoverText)) :
                    echo $hoverText;
                endif;
                ?>
                <a href="<?=$link?>" class="link-stacked"></a>
            </div>
        </div>
    <?php
    elseif($type=="technology"): ?>
        <footer class="square-card__footer right text-right technology">
            <div class="ui-picto ui-picto--sm ui-picto--success">
                <div class="ui-picto__row">
                    <div class="ui-picto__item">
                        <i class="icon icon-plus mr-0"></i>
                    </div>
                    <a href="<?=$link?>" class="link-stacked"></a>
                </div>
            </div>
            </div>
        </footer>
        <div class="square-card__hover-content">
            <div class="square-card__hover-content-inner">
                <?php if(!empty($title)): ?>
                    <div class="mb-2"><?=$title?></div>
                <?php endif; ?>
                <?php if(!empty($hoverText)) :
                    echo $hoverText;
                endif;
                ?>
                <a href="<?=$link?>" class="link-stacked"></a>
            </div>
        </div>
    <?php else: ?>
        <div class="square-card__hover-content">
            <div class="square-card__hover-content-inner">
                <?php if(!empty($title)): ?>
                    <div class="mb-2"><?=$title?></div>
                <?php endif; ?>
                <?php if(!empty($hoverText)) :
                    echo $hoverText;
                endif;
                ?>
                <a href="<?=$link?>" class="link-stacked"></a>
            </div>
        </div>
    <?php
    endif;
    echo ' </div></article></div>';
endwhile;
wp_reset_postdata();