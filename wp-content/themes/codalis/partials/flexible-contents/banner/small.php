<?php
$smallBanner = $block['image'];
$smallBanner = \App\getImageManager()->resize( \App\getImageDirectoryPath($smallBanner), \App\IMAGE_SIZE_SMALL_BANNER);
$text = $block['text'];
?>
<section class="promo text-white is-extended-full">
    <div class="promo__body">
        <?php if(!empty($smallBanner)): ?>
        <figure class="promo__picture mb-0">
            <img alt="Promo Banner" class="promo__img img" src="<?php echo $smallBanner; ?>" />
        </figure>
        <?php endif; ?>

        <?php if(!empty($text)): ?>
        <div class="overlay-container">
            <div class="overlay-card promo__overlay-card right y-center">
                <div class="overlay-card__body py-sm-0">
                    <?php
                    $text = App\app_add_class($text);
                    echo $text;
                    ?>
                    <!--<h1 class="overlay-card__title text-white mb-1">Titre h1 de la page</h1>
                    <p>Lorem ipsum dolor sit amet it conetur empor incididunt aliqua.</p>-->
                </div>
            </div>
        </div>
        <?php endif; ?>

    </div>
</section><!-- /.Banner / Promo section ends -->