<?php
$image = $block['image'];
$image = \App\getImageManager()->resize( \App\getImageDirectoryPath($image), \App\IMAGE_SIZE_FOCUS_BANNER);
if(!empty($image)):
    ?>
    <section class="promo is-extended-full">
        <div class="promo__body">
            <figure class="promo__picture mb-0"><img alt="Image" class="promo__img img img-full" src="<?=$image?>" /></figure>
        </div>
    </section><!-- /.Simple banner section  -->
<?php endif;