<?php
$bannerImage = $block['image'];
$title = $block['title'];
$text = $block['text'];
$introContent = $block['intro_content'];
$bannerImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($bannerImage), \App\IMAGE_SIZE_INTRO_BANNER);
$bannerText = $block['text'];
?>
<section class="promo text-white is-extended-full">
    <div class="promo__body position-relative">
        <?php if(!empty($bannerImage)): ?>
        <figure class="promo__picture mb-0">
            <img alt="Promo Banner" class="promo__img img" src="<?=$bannerImage?>" />
        </figure>
        <?php endif;?>
        <!-- Overlay content -->
        <div class="overlay-container">
            <div class="overlay-card promo__overlay-card right y-center">
                <div class="overlay-card__body py-sm-0">
                    <?php if(!empty($title)): ?>
                    <div class="overlay-card__title h1 text-white"><?=$title?></div>
                    <?php endif; ?>
                     <?php if(!empty($text)) { echo $text; } ?>
                </div>
            </div>
        </div>
    </div>
    <!-- Reusable intro section -->
    <?php if(!empty($introContent)): ?>
    <div class="block block--intro mt-pushed-100">
        <div class="container">
            <div class="box-card-container box-card-container-pad-left bg-gradient-primary is-xs-extended">
                <div class="row no-gutters">
                    <?php if(!empty($introContent['title'])): ?>
                    <div class="col-sm-6 d-flex">
                        <div class="box-card align-items-center">
                            <div class="box-card__body">
                                <h2 class="mb-0"><?=$introContent['title']?></h2>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                    <?php if(!empty($introContent['text'])): ?>
                    <div class="col-sm-6 d-flex">
                        <div class="box-card align-items-center pl-md-6">
                            <div class="box-card__body">
                                <?=$introContent['text']?>
                            </div>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div><!-- /.Reusable intro section ends -->
    <?php endif; ?>
</section><!-- /.Banner / Promo section ends -->