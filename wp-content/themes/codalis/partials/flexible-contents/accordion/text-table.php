<div class="text-card">
    <?php if(!empty($text)):
        echo $text;
    endif; ?>
    <?php if(!empty($table)): ?>
        <table class="table table--bordered table--responsive text-center">
            <tbody>
            <?php
            foreach ( $table['body'] as $tr ) {
                echo '<tr>';
                foreach ( $tr as $td ) {
                    echo '<td data-th-title="'.$td['c'].'">';
                    echo $td['c'];
                    echo '</td>';
                }
                echo '</tr>';
            }
            ?>
            </tbody>
        </table>
    <?php endif;?>
</div>