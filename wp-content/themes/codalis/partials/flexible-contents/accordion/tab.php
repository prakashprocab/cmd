<section class="block block--tab <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <?php if(!empty($block['text'])): ?>
                <div class="mb-5"><?=$block['text']?></div>
        <?php endif; ?>
        <div class="tab-container js-tab-container">
            <nav class="tab-nav d-none d-sm-block" role="navigation">
                <ul class="tab-list" role="tablist">
                    <?php
                    $c=1;
                    foreach ($contents as $content) :
                        $active = !empty($content['active'])?'active':'';
                        ?>
                        <li class="tab-list__item <?=$active?>">
                            <a href="" class="tab-list__link js-tab-toggle" data-tab-target="tab-pane<?=$c?>" target="_blank"><?=$content['title']?></a>
                        </li>
                        <?php $c++; endforeach; ?>
                </ul>
            </nav>
            <div class="tab__content">
                <div class="accordion-container">
                    <div class="accordion accordion--collapsable js-accordion" data-scroll-active="true" data-multiple-option="true">
                        <?php
                        $count=1;
                        foreach ($contents as $content) :
                            $active = "";
                            $activePane = "";
                            $expandable = "";
                            $style='style="display: none;"';

                            if(!empty($content['active'])) :
                                $active = "active";
                                $activePane = "active-pane";
                                $expandable = "expanded";
                                $style='style="display: block;"';
                            endif;

                            $title = $content['title'];
                            $col1 = $content['text'];
                            $col2 = $content['text_2'];
                            ?>

                            <div class="tab__pane tab-pane<?=$count?> <?=$activePane?> animate">
                                <div class="accordion-panel <?=$active?>">
                                    <div class="accordion-panel__header d-block d-sm-none">
                                        <h4 class="accordion-panel__title mb-0"><a href="" class="accordion-panel__link js-accordion__toggle <?=$expandable?>" data-toggle-target="js-accordion-target-panel<?=$count?>" target="_blank"><?=$title?></a></h4>
                                    </div>
                                    <div class="accordion-panel__collapse <?=$style?> js-accordion-target-panel<?=$count?>">
                                        <?php if(!empty($col1) || !empty($col2)): ?>
                                            <div class="accordion-panel__body">
                                                <div class="row">
                                                    <?php if(!empty($col1)): ?>
                                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                                            <div class="text-card">
                                                                <?=$col1?>
                                                            </div>
                                                        </div>
                                                    <?php endif;
                                                    if(!empty($col2)):
                                                        ?>
                                                        <div class="col-sm-6 mb-3 mb-sm-0">
                                                            <div class="text-card">
                                                                <?=$col2?>
                                                            </div>
                                                        </div>
                                                    <?php endif; ?>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <?php $count++; endforeach;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
