<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$text = $block['text_1'];
$title = $block['title'];

?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <?php if(!empty($title)) : ?> <h3 class="entry-card__title"><strong><?=$title?></strong></h3> <?php endif; ?>
        <div class="row row-gutter-30">
            <div class="col-sm-12">
                <?php echo App\app_add_class($text); ?>
            </div>
        </div>
    </div>
</section>
