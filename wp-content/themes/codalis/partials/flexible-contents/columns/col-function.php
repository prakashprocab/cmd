
<?php
/**
 * Created by PhpStorm.
 * User: Kabita
 * Date: 8/27/2018
 * Time: 12:17 PM
 */
/*
 * Image after the text
 */
if (!function_exists('getPatternForBottom')) {
    function getPatternForBottom()
    {
        $start = '><p><img ';
        $end = '/></p>';

        $pattern = sprintf(
            '/%s(.+?)%s/',
            preg_quote($start, '/'), preg_quote($end, '/')
        );

        return $pattern;
    }
}
/*
 *  image is before the text
 */
if (!function_exists('getPatternForTop')) {

    function getPatternForTop()
    {
        $start = '<p><img ';
        $end = '/></p>';

        $pattern = sprintf(
            '/%s(.+?)%s/',
            preg_quote($start, '/'), preg_quote($end, '/')
        );

        return $pattern;
    }
}

/*
 * Get the attached image src
 */
if (!function_exists('getImageSrc')) {
    function getImageSrc($imageAry){
        if(isset($imageAry)) :
            preg_match('/(?<!_)src=([\'"])?(.*?)\\1/', $imageAry, $matchesImageSrc);
            return $matchesImageSrc[0];
        endif;
    }
}

/*
 *  Image pattern and Postion
 * Top or Bottom of Text
 */

if (!function_exists('findImagePattern')) {
    function findImagePattern($str)
    {
//replace linebreaks to calculate patterns
        $str = str_replace("\n", "", $str);
// echo $str;
        $pattern = getPatternForBottom();
        if (preg_match($pattern, $str, $matches)) {
//echo 'found in bottom';
            return [1, $matches[0], $pattern];
        }

        $pattern = getPatternForTop();
        if (preg_match($pattern, $str, $matches)) {
//echo 'found in top';
            return [0, $matches[0], $pattern];
        }

        return null;
    }
}
/*
 * Three Columns
 */

if (!function_exists('displayThreeColumnBlock')) {

    function displayThreeColumnBlock($str)
    {
        $str = str_replace("\n", "", $str);
        $imageAry = findImagePattern($str);
        $str = str_replace('<h4', '<h4 class="entry-card__subtitle"', $str);
        $str = str_replace('<h3', '<h3 class="entry-card__title"', $str);
        $imageSrc = getImageSrc($imageAry[1]);

        if (!$imageAry) return '<article class="entry-card entry-card--flat"><div class="entry-card__body">' . $str . '</div></article>';
        ob_start();
        ?>
        <article class="entry-card entry-card--boxed bg-white">
            <?php
            if ($imageAry[0] == 0):

                if (strpos($imageAry[1],'no-figure') !== false) {
                    //echo 'true is me';
                    $imgHtml = strip_tags($imageAry[1] , "<img>");
                    echo preg_replace('/>/', '', $imgHtml, 1);
                }else {
                    ?>
                    <figure class="entry-card__picture zoom-effect-holder mb-0">
                        <img <?= $imageSrc ?>>
                    </figure>
                    <?php
                }
            endif;
            ?>
            <div class="entry-card__body">
                <?php $str = preg_replace($imageAry[2], '', $str);
                echo $str;
                if (strpos($imageAry[1],'no-figure') !== false) {
                    //echo 'true is me';
                    $imgHtml = strip_tags($imageAry[1] , "<img>");
                    echo preg_replace('/>/', '', $imgHtml, 1);
                }

                ?>
            </div>
            <?php
            if ($imageAry[0] == 1):
                if (strpos($imageAry[1],'no-figure') == false) {
                    ?>
                    <figure class="entry-card__picture zoom-effect-holder mb-0">
                        <img <?= $imageSrc ?>>

                    </figure>
                    <?php
                }
            endif;
            ?>

        </article>

        <?php
        return ob_get_clean();
    }
}

/*
 * Two Columns
 */
if (!function_exists('displayColumnTwoBlock')) {

    function displayColumnTwoBlock($str){
        $str = str_replace("\n", "", $str);
        $imageAry = findImagePattern($str);
        $imageSrc = getImageSrc($imageAry[1]);
        //$imageSrc = \App\getImageManager()->resize( \App\getImageDirectoryPath($imageSrc), \App\IMAGE_SIZE_TWO_COLS);
        $str = str_replace('<h4', '<h4 class="entry-card__subtitle"', $str);
        $str = str_replace('<h3', '<h3 class="entry-card__title"', $str);

        if (!$imageAry) return '<article class="entry-card entry-card--sm entry-card--flat"><div class="entry-card__body">' . $str . '</div></article>';
        ob_start();
        ?>
        <article class="entry-card entry-card--sm entry-card--flat">
            <?php
            if ($imageAry[0] == 0):
                if (strpos($imageAry[1],'no-figure') !== false) {
                    //echo 'true is me';
                    $imgHtml = strip_tags($imageAry[1] , "<img>");
                    echo preg_replace('/>/', '', $imgHtml, 1);
                }else {
                    ?>
                    <figure class="entry-card__picture zoom-effect-holder mb-0">
                        <img class="entry-card__img img img-full" <?php echo $imageSrc; ?>>
                    </figure>
                    <?php
                }
            endif;
            ?>
            <div class="entry-card__body">
                <?php $str = preg_replace($imageAry[2], '', $str);
                echo $str;

                if (strpos($imageAry[1],'no-figure') !== false) {
                    //echo 'true is me';
                    $imgHtml = strip_tags($imageAry[1] , "<img>");
                    echo preg_replace('/>/', '', $imgHtml, 1);
                }
                ?>
            </div>
            <?php
            if ($imageAry[0] == 1):
                    if (strpos($imageAry[1],'no-figure') == false) {
                        ?>
                        <figure class="entry-card__picture zoom-effect-holder mb-0">
                            <img class="entry-card__img img img-full " <?php echo $imageSrc; ?>>
                        </figure>
                        <?php
                    }
                ?>
                <?php
            endif;
            ?>
        </article>
        <?php
        return ob_get_clean();
    }
}
