<?php
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = $block['background_color'];
$title = $block['title'];
 $text1 = $block['text_1'];
$text2 = $block['text_2'];

?>
<section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
    <div class="block__body">
        <?php if(!empty($title)) : ?> <h3 class="entry-card__title"><strong><?=$title?></strong></h3> <?php endif; ?>
        <div class="row">

            <div class="col-sm-4 mb-4 mb-sm-0">
                <article class="entry-card">
                    <?php if(!empty($text1)) :
                        echo $text1;
                    endif;
                    ?>
                </article>
            </div>

            <div class="col-sm-8">
                <article class="entry-card">
                    <div class="entry-card__body py-0 pl-0">
                        <?php   echo $text2; ?>
                    </div>
                </article>
            </div>

        </div>
    </div>
</section>