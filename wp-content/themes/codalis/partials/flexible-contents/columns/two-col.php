
<?php
/**
 * https://stackoverflow.com/questions/6045607/search-for-a-string-in-a-php-file-with-a-start-and-end
 */
$spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
$bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
$title = $block['title'];

?>
    <section class="block <?=$bgColor?> <?=$spaceType?> is-extended">
        <div class="block__body">
            <?php if(!empty($title)) : ?> <h3 class="entry-card__title"><strong><?=$title?></strong></h3> <?php endif; ?>
            <div class="row row-gutter-30">
                <div class="col-sm-6 mb-4 mb-sm-0">
                    <?php echo displayColumnTwoBlock($text_1); ?>
                </div>
                <div class="col-sm-6">
                    <?php echo displayColumnTwoBlock($text_2); ?>
                </div>
            </div>
        </div>
    </section>