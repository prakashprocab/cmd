<?php
    $spaceType = empty($block['space_type'])?'spacing-py-eq':$block['space_type'];
    $bgColor = empty($block['background_color'])?'bg-faded':$block['background_color'];
    $images = $block['image'];
    $random_image = array_rand($images,1);
    $quote = $block['quote'];
    $bannerImage = $block['banner_image'];
    $bannerImage = \App\getImageManager()->resize( \App\getImageDirectoryPath($bannerImage), \App\IMAGE_SIZE_FOCUS_BANNER);
  $randomeImg = $images[$random_image]['image'];
  $randomeImg = \App\getImageManager()->resize( \App\getImageDirectoryPath($randomeImg), \App\IMAGE_SIZE_SERVICE);
?>
    <section class="stacked-block stacked-block--promo <?=$bgColor?> <?=$spaceType?>">
    <div class="stacked-block__body">
        <?php if(!empty($bannerImage)): ?>
            <div class="stacked-promo is-extended-full">
                <figure class="stacked-promo__picture mb-0"><img alt="Image" class="stacked-promo__img img img-full" src="<?=$bannerImage?>" /></figure>
            </div>
        <?php endif; ?>
        <div class="stacked-card-container">
            <?php if(!empty($randomeImg)): ?>
            <div class="stacked-card-col stacked-card-col--picture mt-pushed-350">
                <div class="stacked-card is-shadowed">
                    <div class="stacked-card__body p-0">
                        <figure class="stacked-card__picture mb-0">
                            <img alt="Picture" class="stacked-card__img img" src="<?=$randomeImg?>" />
                        </figure>
                    </div>
                </div>
            </div>
            <?php endif;

            if(!empty($quote)): ?>
                <div class="stacked-card-col stacked-card-col--text align-self-center">
                    <div class="stacked-card stacked-card--sm is-xs-shadowed">
                        <div class="stacked-card__body">
                            <blockquote><p>« <?=$quote?>  »</p></blockquote>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</section>
