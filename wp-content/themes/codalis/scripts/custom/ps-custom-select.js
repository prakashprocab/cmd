/**
* @name: ps-custom-seelct.js
* @desc: initialize instance of select2
*/

/**
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

;(function($) {    
    window.buildSelect2 = function() {
        // Caching DOM elements which are being used in filter to avoid frequent DOM traverse
        var $selectBox = $('.js-custom-control');
        // $selectBox.hide();
        if($selectBox.length === 0) return;

        // Inject select2 plugin to $select
        $selectBox.select2({
            width: 'resolve',
            // maximumSelectionLength: 10,
            minimumResultsForSearch: 10,
            adaptDropdownCssClass: function(c) {
              // Pass the select's classes through to the dropdown.
              return c;
            },
        })
        .on('select2:open', function() {
            // Injecting nicescroll in select2
            $('.select2-results__options').niceScroll({
                cursorcolor: "#cac5c4",
                cursoropacitymin: 0.5,
                background: "#dedad9",
                cursorborder: "0",
                autohidemode: false,
                cursorwidth: 10,
                cursorborderradius: 0,
                cursorminheight: 80,
                cursorfixedheight: 80,
                touchbehavior: false,
                hwacceleration: true,
                preservenativescrolling: false,
            });

            //        
            var itemsLen = $(this).find('> option').length;
            if(itemsLen > 10) {
                $('.select2-dropdown').addClass('select2-search-enabled');
                $('.select2-results__options').addClass('select2-results__options--lg');
            }

            // // Updating position of scrollbar on window resize
            $('.select2-results__options').getNiceScroll().show().resize();

            $('.select2-results__options').on("wheel", function ( e ) {
                var event = e.originalEvent,
                d = -event.deltaY || -event.detail ;

                this.scrollTop += ( d < 0 ? 1 : -1 ) * 30;
                e.preventDefault();
                return false;
            });
        });
    }
    
    // Initialize Select 2
    window.buildSelect2();
})(jQuery);