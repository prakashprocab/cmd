/**
* @name: ps-counter-slider.js
* @desc: creating slider using swiper's library
*/

/**
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// Wrap javascript code inside a clouser to avoid possible conflicts with other codes
;(function($) {
	// == Enable strict mode using use-strict directive
	"use strict";

	// == Helper function to display current slides by total slides
	function _ps_countSlides(_swiper, $target) {
        var slideCount = _swiper.slides.length - 2, // subtracting total sildes length by 2 to fix loop mode issue
            currentSlide = parseInt((_swiper.activeIndex - 1) % (_swiper.slides.length - 2)) + 1; // gets current slide index

		if (currentSlide === 0) currentSlide = slideCount; // Fix the issue when navigation slider with previous button

        // Update current slide count
        $target.find('.js-current-slide').html(currentSlide);
        $target.find('.js-total-slide').html(slideCount);
	}

    // == Initialize standalone slider instance
    _ps_counterSlider($('.js-counter-slider'));

	// == Build standalone slider
	function _ps_counterSlider($node) {
		if($node.length === 0) return;
		$node.each(function() {
			var $self = $(this);
			var $counterNode = $self.closest('.js-counter-slider-parent').find('.js-counter-slider-count');

			var counterSwiper = new Swiper($self, {
				autoplay: false,
				loop: true,				
				navigation: {
	                nextEl: $self.closest('.js-counter-slider-parent').find('.js-counter-swiper-button-next'),
	                prevEl: $self.closest('.js-counter-slider-parent').find('.js-counter-swiper-button-prev'),
	            },
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					type: 'bullets',
				},
				speed: 500,
				spaceBetween: 0,
                on: {
                    init: function () {
                        _ps_countSlides(this, $counterNode);

                        this.$el[0].classList.add('swiper-initialized');
                    },
                },
			});

            counterSwiper.on('slideChange', function() {
                _ps_countSlides(this, $counterNode);
			});
		});
	}
})(jQuery); // Self invoking function - IIFE