/*!
* @name: ps-custom-tab.js
* @desc: Build tabify UI interface
*/

/*!
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function($) {
	/**
    * Don't write any code above this except comment because "use strict" directive won't have any effetc
    * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
    */
	"use strict";
	
	// Initialize jqueryTabUI();
	_ps_jqueryCustomTab($('.js-tab-toggle'));

	function _ps_jqueryCustomTab($tabUi) {
		if($tabUi.length === 0) return;
		$tabUi.each(function() {
			var $self = $(this),
				$selfParentLi = $self.closest('li'),
				$selfParent = $self.closest('.js-tab-container'),
				$tabContent = $selfParent.find('.tab__content');

			//Attach click event on each tab ul list item
			$self.on('click', function(e) {
				if(!$selfParentLi.hasClass('active')) {
					//Add active class to current instant of each click list item
					$selfParentLi.addClass('active');
					//Remove active class from all other list items
					$selfParentLi.siblings().removeClass('active');
					//Adding class to related active tab panel and removing from all others tab panel
					$tabContent.find('.tab__pane').removeClass('active-pane animate');
					$tabContent.find($('.' + $self.attr('data-tab-target'))).addClass('active-pane');
					setTimeout(function() {
						$tabContent.find('.active-pane').addClass('animate');
					}, 10);
				}				
				//Prevent Deafult behavior
				e.preventDefault();
			});
		});
	}
})(jQuery);