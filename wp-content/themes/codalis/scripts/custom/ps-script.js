/**
* @name: ps-script.js
* @desc: includes common javascript snippet necessary to run over the page
*/

/**
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == Resolve possible jQuery conflict with '$'
var $ = jQuery.noConflict();

// == Caching Global DOM NODES
var $win = $(window);
var $page = $('html, body');
var $doc = $('body');
var $header = $('#mastHead');
var headerHeight = $header.outerHeight();
var winTimer = null;
var winTimerDelay = 10;
var docClickFlag = false;
// var clickScrollStatus = false;

// == Stuffs to be reused in other scripts

/**
* Check the device type
*/
window.isTouchDevice = function() {
	var check = false;
	(function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
	return check;
};

/**
* Check if device is touch(ipad or below ipad) or not
*/
var mblBreakpoint = null;
var laptopBreakpoint = null;
if (isTouchDevice()) {
	mblBreakpoint = 767;
	laptopBreakpoint = 1280;
} else {
	mblBreakpoint = 767 - 17;
    laptopBreakpoint = 1280;
}

// == Update scrollbar value on device
var scrollbarThreshold = 0;
if (!Modernizr.touch) scrollbarThreshold = 17;

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function(w, d, b, $, undefined) {
	/**
    * Don't write any code above this except comment because "use strict" directive won't have any effetc
    * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
    */
	"use strict";
	
	/* ================ Commonly used javascript codes ================ */

	/**
	* Add user-agent as class
	*/
	d.documentElement.setAttribute('data-useragent', navigator.userAgent);

	/**
	* Code once DOM is ready
	*/
	$(function() {
		// Add class to body once DOM is ready
		$(b).addClass('content-loaded');
	});

	/**
	* Code once window loads
	*/
	$(w).on('load', function() {
		// == Add class to body on window load
		$(b).addClass('page-loaded');
	});

	/**
	* Add classes according to devices type (touch or not)
	*/

	// == Init deviceDetection() on page load
	deviceDetection();

	/**
	* == Build deviceDetection function
	*/
	function deviceDetection() {
		var deviceType = window.isTouchDevice();
		if (deviceType === true) {
			d.documentElement.classList.add('isTouchDevice');
			d.documentElement.classList.remove('notTouchDevice');
		}else {
			d.documentElement.classList.add('notTouchDevice');
			d.documentElement.classList.remove('isTouchDevice');
		}
	}

	/*
	** Add heloper class on DOM element on window resize
	*/
	function activateResizeHandler() {	    
	    var resizeClass = 'resize-active',
	    flag, timer;

	    var removeClassHandler = function() {
	      flag = false;
	      d.documentElement.classList.remove(resizeClass);

	    };

	    var resizeHandler = function() {
	      if(!flag) {
	        flag = true;
	        d.documentElement.classList.add(resizeClass);
	      }
	      clearTimeout(timer);
	      timer = setTimeout(removeClassHandler, 10);
	    };
	    $(w).on('resize orientationchange', resizeHandler);
  	};

  	// == activateResizeHandler initialization
	activateResizeHandler();

    /**
	 * @desc sticky header + request to quote button's visibility
     */

    var $jsRequestQuoteNode = $('.js-request-quote-target');
    $(w).scroll(function (event) {
        var $self = $(this);
        var scrollTop = $self.scrollTop();

        /* =========== Home page sticky header feature ============ */

        // Toggle class for sticky header
        if ($doc.hasClass('home')) {
            if (scrollTop >= 20) {
                if (window.docClickFlag === false) $header.addClass('is-stuck');
            } else {
                $header.removeClass('is-stuck');
            }
        }

        if ($doc.hasClass('home')) {
            // Request a quote sticky feature
            $(function() {
                if (scrollTop >= $jsRequestQuoteNode.offset().top + $jsRequestQuoteNode.outerHeight() - $header.outerHeight()) {
                    $doc.addClass('is-request-quote-shown');
                } else {
                    $doc.removeClass('is-request-quote-shown');
                }
            });
        }
    });

    /**
     * Project specific javascript code to fix header overflow issue which has been set deliberately to fix submenu issues
     */

    $('.lang-dropdown .dropdown-toggle').on('click', function(e) {
        // == Fix overlapping issue caused by submenus
        if (!$(this).parent().hasClass('show')) {
            $header.addClass('is-submenu-shown');
        } else {
            setTimeout(function() {
                $header.removeClass('is-submenu-shown');
            }, 350);
        }
    });

    $('.lang-dropdown').on('hidden.bs.dropdown', function() {
        setTimeout(function() {
            $header.removeClass('is-submenu-shown');
        }, 350);
	});

    $('.lang-dropdown').on('shown.bs.dropdown', function() {
    	if ($header.hasClass('is-submenu-shown')) {
            setTimeout(function () {
                $('.has-submenu').removeClass('is-already-shown is-shown');
            }, 350);
        }
    });
    
    /**
    * LightGallery initialization
    */

	// Lightbox gallery function
	window.buildLightBoxVideo = function(__node) {
		if(__node.length === 0) return;
		__node.each(function() {
			var $self = $(this);
			$self.lightGallery({
				videoMaxWidth: 75 + '%',
                youtubePlayerParams: {
                    modestbranding: 0,
                    showinfo: 0,
                    rel: 0,
                },
                vimeoPlayerParams: {
                    byline : 0,
                    portrait : 0,
                    color : 'A90707'
                }
			});
		});
	};

    window.buildLightBoxVideo($('.js-lightbox'));

	/**
	* CMS stuffs
	*/

	// == Add helper classes to list that comes from editor
	detectListType($('ol, ul'));

	function detectListType(_list) {
	    if($(_list).length === 0) return;
	    $(_list).each(function() {
	        var $self = $(this);
	        if($self.attr('class') === undefined) {
	            if($self.is('ol')) {
	                $self.addClass('lists-editor lists-editor--ordered');
	            }else if($self.is('ul')) {
	                $self.addClass('lists-editor lists-editor--unordered');
	            }
	        }
	    });
	}

    /**
    * Back to top of the page script
    */

    // == Init back to top function
    scrollPageTop($('.js-scroll-top-trigger'));

    // == Build a function to scroll at the top of the page
    function scrollPageTop($elem, $parent) {
        if($elem.length === 0) return;

        $win.on('scroll', function() {
            var pageTop = $(this).scrollTop();

            if (pageTop >= 200) {
                $elem.parent().addClass('is-shown');
                $doc.addClass('is-scroll-top-shown');
            } else {
                $elem.parent().removeClass('is-shown');
                $doc.removeClass('is-scroll-top-shown');
            }
        });

        //Attach click event on current object
        $elem.on('click', function(e) {

            var scrollDuration = null;

            if (window.innerWidth > window.mblBreakpoint) {
                scrollDuration = 500;
            } else if (window.innerWidth <= window.mblBreakpoint) {
                scrollDuration = 0;
            }

            $page.animate({scrollTop: 0}, {duration: scrollDuration, easing: 'easeInOutExpo'});
            scrollDuration = null;
            // Prevent the default behavior of the anchor tag
            e.preventDefault();
        });
    }

    /**
     * Scroll to target section with matching href and id
     */

    $('a[href^="#"]:not(.js-menu-link):not(.js-submenu-link)').each(function() {
    	var $self = $(this);
        $self.on('click', function(e) {
            var threshold = null;

            if ($self.attr('href') === "#") return;

            if ($self.offset().top >= $(this.hash).offset().top) {
                threshold = window.headerHeight;
            }

            $page.animate(
                {
                    scrollTop: $(this.hash).offset().top + 1 - threshold
                },
                {
                    duration: 700, easing: 'easeInOutExpo'
                }
            );
            e.preventDefault();
        });
    });

    /**
	* Fancy form placeholder
	*/

	function fancyFormPlaceholder(_node) {
		if (_node.length === 0) return;
		_node.each(function() {
			var $self = $(this);
			$self.on('focus', function(e) {
				$self.closest('.form-group').addClass('focused');
			});
			$self.on('blur', function(e) {
				$self.closest('.form-group').removeClass('focused');
			});
			$self.on('keyup', function() {
				if($self.val().length > 0) {
					$self.closest('.form-group').addClass('filled');
				}else {
					$self.closest('.form-group').removeClass('filled');
				}
			});
		});
	}

	fancyFormPlaceholder($('.js-form-control'));

    /**
	 * From input fields placeholder script
	 */
    window.placeholderEffect = function(input)  {
        if($(input).length === 0) return;
        input.each( function(){
            var meInput = $(this);
            var placeHolder = $(meInput).attr('placeholder');
            $(meInput).focusin(function(){
                $(meInput).attr('placeholder','');
            });
            $(meInput).focusout(function(){
                $(meInput).attr('placeholder', placeHolder);
            });
        });
    }

    /*
    ** Placeholder initialization
    */
    window.placeholderEffect($('.js-form-control'));

	/**
	 * Detecting elements in each last row to add helper classes to the detect elements
	 */

    window.getLastRowCols = function (container, elem) {
        container.each(function() {
            var $self = $(this);
            var colInRow = 0;
            //var $container = null;
            $self.find(elem).each(function() {
                if($(this).prev().length > 0) {
                    if($(this).position().top != $(this).prev().position().top) return false;
                    colInRow++;
                }
                else {
                    colInRow++;
                }
                var colInLastRow = $self.find(elem).length % colInRow;
                if(colInLastRow == 0) colInLastRow = colInRow;
                $self.find(elem).addClass('mb-4 col-has-mb')
                $self.find(elem).slice(-colInLastRow).removeClass('mb-4 col-has-mb')
            });
        });
	};

    window.getLastRowCols($('.js-row'), $('.js-last-col'));

    /**
	 * Function to init Media Element instances
     */
    _ps_initMediaElement($('.js-audio-node'));

	function _ps_initMediaElement(_node) {
		if(_node.length === 0) return;
        _node.each(function() {
			var $self = $(this);
			$self.mediaelementplayer();
		});
	}

	/**
	 * Custom nicescroll
	 */
	window.customNiceScroll = function(__node) {
		if (__node.length === 0) return;
		__node.niceScroll({
			cursorcolor: "#cac5c4",
			cursoropacitymin: 0.5,
			background: "#dedad9",
			cursorborder: "0",
			autohidemode: false,
			cursorwidth: 10,
			cursorborderradius: 5,
			cursorminheight: 50,
			cursorfixedheight: 50,
			hwacceleration: true,
			preservenativescrolling: false,
		});
	
		// == Updating position of scrollbar on window resize
		__node.getNiceScroll().show().resize();
	};

	if (!window.isTouchDevice()) {
		window.customNiceScroll($('.js-nicescroll-node'));
	}

  	/**
  	* Recalculate calculations on window resize
  	*/
	$(w).on('resize', function() {
		if(winTimer != null) clearTimeout(winTimer);
		winTimer = setTimeout(function() {

			// == Re-Init deviceDetection() on page load
			deviceDetection();

			// Update header height on window resize
			headerHeight = $header.outerHeight();
			
			// Init getLastRowCols
			getLastRowCols($('.js-row'), $('.js-last-col'));

			if (!window.isTouchDevice()) {
				window.customNiceScroll($('.js-nicescroll-node'));
			}

			// Calculate and update mega menu height on window resize
            window.ps_constructSubMenu($('.js-submenu-link'), $('.js-submenu-content-container'), $('.submenu-holder')); // referenced from ps-custom-navbar.js
		}, winTimerDelay);
	});

})(window, document, 'body', jQuery, undefined);

/**
* Wow plugin initialization
*/
;(function() {
	// wow = new WOW({
	// 	boxClass:     'wow',      // default
	// 	animateClass: 'animated', // default
	// 	offset:       0,          // default
	// 	mobile:       false,       // default
	// 	live:         true        // default
	// });
    //
	// wow.init();
})();