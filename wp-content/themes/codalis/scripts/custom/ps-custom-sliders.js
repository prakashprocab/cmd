/**
* @name: ps-custom-sliders.js
* @desc: creating slider using swiper's library
*/

/**
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// Wrap javascript code inside a clouser to avoid possible conflicts with other codes
;(function($) {
	// == Enable strict mode using use-strict directive
	"use strict";

	// == Build standalone slider
	window.initSlider = function($node) {
		if($node.length === 0) return;
		$node.each(function() {
			var $self = $(this);
			var isautoplayOn = $self.attr('data-autoplay') !== undefined ? true : false;
			var autoPlayOption = {};

			if (isautoplayOn) {
				autoPlayOption =  {
					delay: $self.attr('data-autoplay'),
					disableOnInteraction: false
				};
			} else {
				autoPlayOption = isautoplayOn;
			}

			var swiper = new Swiper($self, {
				autoplay: autoPlayOption,
				autoHeight: true,
				loop: true,				
				navigation: {
	                nextEl: '.swiper-button-next',
	                prevEl: '.swiper-button-prev',
	            },
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
					type: 'bullets',
				},
				speed: 500,
				spaceBetween: 0,
			});
		});
	}

	// == Initialize standalone slider instance
	window.initSlider($('.js-slider-hero'));
})(jQuery); // Self invoking function - IIFE