/**
* @name: ps-custom-collapse.js
* @desc: Build collapsable UI interface
*/

/**
* Wrap all javascript snippets inside a javascript clousure
* to avoid potential code conflict with
* other libraries and codes ***
* ';' infront of the IIFE is for security purpose, i.e. to run our code in safe mode.
* Because when we try to compress/optimize our js code, compiler/optimizer try to concatenate our code with other js codes
* and it throws an error if it detects a statement without ending with ';' and breaks whole code!
* To fix this issue, it's always safe to add ';' infront of our IIFE which ensures compiler won't throw any error even if a statement doesn't ends at ';'
*/

// == This is an Immediately Invoked Function Expression (IIFE) or simply known as Self-Invoking Function
;(function($) {
    /**
    * Don't write any code above this except comment because "use strict" directive won't have any effetc
    * Enable strict mode to prevent script to run in safe mode (modern way - ECMA2016)
    */
    "use strict";

    var $header = $('#mastHead');
    
    // == Init _ps_jqueryCustomAccordion
    _ps_jqueryCustomAccordion($('.js-accordion__toggle'));

    // == Helper function to scroll to the top of the parent on click on item
    function _ps_scrollTopParent(_parent) {
        setTimeout(function() {
            $('html, body').animate({
                scrollTop: $(_parent).offset().top - $header.outerHeight() + 1
            }, {
                duration: 1200,
                easing: 'easeInOutExpo'
            });
        }, 500);
    }

	// Function to create accordion UI
    function _ps_jqueryCustomAccordion($item) {
        if($item.length === 0) return;    
        
        $item.each(function() {
            var $self = $(this),
                dataScrollActive = ((($self.closest('.js-accordion').attr('data-scroll-active')) !== undefined || ($self.closest('.js-accordion').attr('data-scroll-active')) !== '') && ($self.closest('.js-accordion').attr('data-scroll-active')) === 'true') ? true : false,
                dataMultiOpen = ((($self.closest('.js-accordion').attr('data-multiple-option')) !== undefined || ($self.closest('.js-accordion').attr('data-multiple-option')) !== '') && ($self.closest('.js-accordion').attr('data-multiple-option')) === 'true') ? true : false;

            // Prepare accordion before initialization
            $('.accordion-panel:not(.active)').find('.accordion-panel__collapse').css('display', 'none');
            $('.accordion-panel.active').find('.accordion-panel__collapse').css('display', 'block');

            //Attach click event
            $self.on('click', function(e) {
                var $target, $selfParent;
                $target = $self.closest('.accordion-panel').find($('.' + $self.attr('data-toggle-target')));
                $selfParent = $self.closest('.accordion-panel');

                // Reset all other panels
                if (dataMultiOpen === true) {
                    $self.closest('.js-accordion').find('.js-accordion__toggle').not($self).removeClass('expanded');
                    $self.closest('.js-accordion').find('.accordion-panel__collapse').not($target).slideUp({
                        duration: 500,
                        easing: 'easeInOutExpo'
                    });
                    $self.closest('.js-accordion').find('.accordion-panel.active').not($selfParent).removeClass('active');
                }

                // Expand current panel
                $self.toggleClass('expanded');
                $target.stop(true, true).slideToggle({
                    duration: 700,
                    easing: 'easeInOutExpo'
                });
                $selfParent.toggleClass('active');

                // Move page to the top offset of currently opened panel
                if($self.hasClass('expanded') && dataScrollActive === true) {
                    _ps_scrollTopParent($selfParent);
                }
                
                // Prevent default behavior of anchor tag
                e.preventDefault();
            });
        });        
    }
})(jQuery);